import { Meteor } from 'meteor/meteor';
import React from 'react';
import { render } from 'react-dom';

import App from '../imports/ui/App';

console.log(process.env);
Meteor.startup(() => {
  render(<App />, document.getElementById('app'));
});
