import AWS from 'aws-sdk';
import generatePassword from 'password-generator';

export const uploadImage = imageBase64Data => {
  const cloudfrontURL = 'https://dko0rpcmkcqre.cloudfront.net/';
  const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_KEY_ID,
    secretAccessKey: process.env.AWS_ACCESS_KEY
  });
  const imageName = generatePassword(9, false, /[\d\w]/);
  const imageFormat = imageBase64Data.split(';')[0].split('/')[1];
  const imageBase64 = imageBase64Data.split(',')[1];
  const imageBuffer = Buffer.from(imageBase64, 'base64');
  const image = `${imageName}.${imageFormat}`;
  const params = {
    Bucket: 'updatefitness',
    Body: imageBuffer,
    Key: `images/${image}`
  };
  return new Promise((resolve, reject) => {
    s3.upload(params, (err, data) => {
      if (err) {
        console.log('saving image error', err);
        reject(err);
        return;
      }
      console.log('saving image data', data);
      resolve(`${cloudfrontURL}${image}`);
    });
  });
};
