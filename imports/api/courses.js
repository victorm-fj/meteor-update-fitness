import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { HTTP } from 'meteor/http';
// import { Dates } from './dates';

export const Courses = new Mongo.Collection('courses');

const COURSES_URL =
  'https://suiteint.update-fitness.ch/index.php?entity=course&columnnames=no,name,course_type,location_name,issuer,startdate,enddate,personno,firstname,lastname,remark&idcolumnname=id&tablename=course&calling_function=tab_page.load_maingrid&format=json';

if (Meteor.isServer) {
  Meteor.publish('courses', function() {
    return Courses.find();
  });
}

Meteor.methods({
  'courses.sync'() {
    if (!this.userId) throw new Meteor.Error('not-authorized');
    this.unblock();

    return new Promise(resolve => {
      HTTP.post(
        COURSES_URL,
        {
          params: {
            username: 'service',
            password: '20Service17!',
          },
        },
        (error, result) => {
          const courses = JSON.parse(result.content);
          Courses.remove({});
          courses.forEach(course => Courses.insert(course));
          resolve(true);
        }
      );
    });
  },
});

// const response_type = {
//   id: '0bcc2de2-a06a-2f6a-f911-1ff93069bb23',
//   id_course_type: '9298afa6-3a7f-271f-0a22-ccdf6c070d7c',
//   name: 'uu Dance Step',
//   course_type: 'Dance Step',
//   id_person: null,
//   no: '196',
//   licence_no: null,
//   startdate: null,
//   enddate: null,
//   personno: null,
//   firstname: null,
//   lastname: null,
//   issuer: null,
//   remark: null,
//   location_name: 'Uzwil',
//   location_personno: '2',
//   id_location: 'e9755317-6319-746c-dbe3-c37d5fc752fe',
//   duration_minutes: null,
//   default_duration_minutes: 60,
// };
