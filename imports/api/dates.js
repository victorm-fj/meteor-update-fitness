import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { HTTP } from 'meteor/http';
import { rangeWeek } from './helpers';

export const Dates = new Mongo.Collection('dates');

const DATES_URL =
	'https://suiteint2.update-fitness.ch/?entity=course_calendar&format=json';

if (Meteor.isServer) {
  Meteor.publish('dates', function() {
    return Dates.find();
  });

  Meteor.methods({
    'dates.sync'() {
      if (!this.userId) throw new Meteor.Error('not-authorized');
      this.unblock();

      const weekRange = rangeWeek();
      const startDate = weekRange.start.getTime();
      const endDate = weekRange.end.getTime();

      return new Promise(resolve => {
        HTTP.post(
          DATES_URL,
          {
            params: {
              username: 'service',
              password: '20Service17!'
            }
          },
          (error, result) => {
            const dates = JSON.parse(result.content);
            console.log(dates.length);
            Dates.remove({});
            console.log(dates[0]);

            const filteredDates = dates.filter(date => {
              const startdate = new Date(date.startdate).getTime();
              return startdate >= startDate && startdate <= endDate;
            });

            console.log(filteredDates);
            filteredDates.forEach(date => Dates.insert(date));
            // dates.forEach(date => {
            //   const startdate = new Date(date.startdate).getTime();
            //   const formattedDate = { ...date, startdate };
            //   return Dates.insert(formattedDate);
            // });
            resolve(true);
          }
        );
      });
    }
  });
}

// const responseType = {
//   id_course: '9780e828-e4c7-42ee-bf44-2bd2e133614e',
//   id: 'ef9acb80-ab87-6188-7702-6769a3cd81f9',
//   startdate: '2017-05-21 08:30:00+02',
//   enddate: null,
//   remark: null,
// };
