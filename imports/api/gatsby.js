import { HTTP } from 'meteor/http';

const buildWebHookURL =
	'https://api.netlify.com/build_hooks/5b467a91dd28ef352a7dae5a';

export const deployGatsbySite = () => {
	return new Promise((resolve, reject) => {
		HTTP.post(buildWebHookURL, {}, (error, result) => {
			if (error) {
				reject(error);
				return;
			}
			resolve(result);
		});
	});
};
