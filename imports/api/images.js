import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import moment from 'moment';
import { uploadImage } from './awsActions';
import { asyncForEach } from './helpers';

export const Images = new Mongo.Collection('images');

if (Meteor.isServer) {
	Meteor.publish('images', function() {
		return Images.find();
	});
	Meteor.methods({
		async 'images.upload'(image, page, component) {
			if (!this.userId) {
				throw new Meteor.Error(403, 'Not authorized to perform this action');
			}
			try {
				const imageURL = await uploadImage(image.imgSrc);
				console.log('upload imageURL', imageURL);
				const selector = { page, component };
				const modifier = {
					page,
					component,
					imageUrl: imageURL,
					updatedAt: moment().valueOf(),
				};
				Images.upsert(selector, { $set: modifier });
				return;
			} catch (error) {
				console.log('upload error', error);
				throw new Meteor.Error(500, 'Something went wrong, try again');
			}
		},
		async 'images.uploadMultiple'(images, page, component) {
			if (!this.userId) {
				throw new Meteor.Error(403, 'Not authorized to perform this action');
			}
			try {
				const sliders = [];
				await asyncForEach(images, async image => {
					if (image.imgSrc.indexOf('http') >= 0) {
						sliders.push({ imageUrl: image.imgSrc });
					} else {
						const imageUrl = await uploadImage(image.imgSrc);
						console.log('upload imageUrl', imageUrl);
						sliders.push({ imageUrl });
					}
				});
				const selector = { page, component };
				const modifier = {
					page,
					component,
					sliders,
					updatedAt: moment().valueOf(),
				};
				Images.upsert(selector, { $set: modifier });
				return;
			} catch (e) {
				console.log('images.uploadMultiple', e);
				throw new Meteor.Error(500, 'Something went wrong, try again');
			}
		},
		async 'images.uploadAndReturnURL'(picture) {
			if (!this.userId) {
				throw new Meteor.Error(403, 'Not authorized to perform this action');
			}
			try {
				const imageURL = await uploadImage(picture.imgSrc);
				console.log('upload imageURL', imageURL);
				const document = {
					imageUrl: imageURL,
					updatedAt: moment().valueOf(),
				};
				Images.insert(document);
				return imageURL;
			} catch (error) {
				console.log('upload error', error);
				throw new Meteor.Error(500, 'Something went wrong, try again');
			}
		},
	});
}
