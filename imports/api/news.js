import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import moment from 'moment';

export const News = new Mongo.Collection('news');

if (Meteor.isServer) {
  Meteor.publish('news', function() {
    return News.find();
  });
}

Meteor.methods({
  'save.news'(page, title, body) {
    if (!this.userId) {
      throw new Meteor.Error(403, 'Not authorized to perform this action');
    }

    new SimpleSchema({
      page: {
        type: String,
      },
      title: {
        type: String,
      },
      body: {
        type: String,
      },
    }).validate({ page, title, body });

    const doc = {
      title,
      body,
      location: page,
      updatedAt: moment().valueOf(),
      valid_from: null,
      valid_to: null,
      order: 0,
    };

    News.insert(doc);
  },

  'update.order'(_id, inc) {
    if (!this.userId) {
      throw new Meteor.Error(403, 'Not authorized to perform this action');
    }

    // new SimpleSchema({
    //   _id: {
    //     type: String,
    //   },
    //   inc: {
    //     type: Number,
    //   },
    // }).validate({ _id, inc });

    News.update(_id, { $inc: { order: inc } });
  },

  'update.title'(_id, title) {
    if (!this.userId) {
      throw new Meteor.Error(403, 'Not authorized to perform this action');
    }

    // new SimpleSchema({
    //   page: {
    //     title: String,
    //   },
    // }).validate({ title });

    News.upsert(_id, { $set: { title } });
  },

  'update.body'(_id, body) {
    if (!this.userId) {
      throw new Meteor.Error(403, 'Not authorized to perform this action');
    }

    // new SimpleSchema({
    //   page: {
    //     body: String,
    //   },
    // }).validate({ body });

    News.upsert(_id, { $set: { body } });
  },

  'remove.news'(_id) {
    if (!this.userId) {
      throw new Meteor.Error(403, 'Not authorized to perform this action');
    }

    News.remove(_id);
  }
});
