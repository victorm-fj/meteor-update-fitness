import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import moment from 'moment';
import sanitizeHtml from 'sanitize-html';

export const Pages = new Mongo.Collection('pages');

if (Meteor.isServer) {
  Meteor.publish('pages', function() {
    return Pages.find();
  });
}

Meteor.methods({
  'save.content'(page, component, htmlContent) {
    if (!this.userId) {
      throw new Meteor.Error(403, 'Not authorized to perform this action');
    }

    new SimpleSchema({
      page: {
        type: String
      },
      component: {
        type: String
      },
      htmlContent: {
        type: String
      }
    }).validate({ page, component, htmlContent });

    // Sanitize HTML content
    const cleanHtml = sanitizeHtml(htmlContent, {
      allowedTags: sanitizeHtml.defaults.allowedTags.concat(['h2', 'img', 'iframe']),
      allowedAttributes: {
        a: ['href', 'name', 'target', 'style'],
        img: ['src', 'alt', 'width', 'height', 'style'],
        p: ['style'],
        h1: ['style'],
        h2: ['style'],
        h3: ['style'],
        h4: ['style'],
        iframe: ['src', 'width', 'align', 'name', 'sandbox']
      }
    });

    const selector = { page, component };
    const modifier = {
      page,
      component,
      htmlContent: cleanHtml,
      updatedAt: moment().valueOf()
    };

    Pages.upsert(selector, { $set: modifier });
  },

  'save.link'(page, component, link) {
    if (!this.userId) {
      throw new Meteor.Error(403, 'Not authorized to perform this action');
    }

    new SimpleSchema({
      page: {
        type: String
      },
      component: {
        type: String
      },
      link: {
        type: String
      }
    }).validate({ page, component, link });

    const selector = { page, component };
    const modifier = {
      link,
      updatedAt: moment().valueOf()
    };

    Pages.upsert(selector, { $set: modifier });
  },

  'save.editorState'(page, component, editorState) {
    if (!this.userId) {
      throw new Meteor.Error(403, 'Not authorized to perform this action');
    }

    const selector = { page, component };
    const modifier = { content: editorState, updatedAt: moment().valueOf() };
    Pages.upsert(selector, { $set: modifier });
  }
});
