import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { HTTP } from 'meteor/http';

export const Standortes = new Mongo.Collection('standortes');

const STANDORTES_URL =
  'https://suitedev.update-fitness.ch/index.php?entity=location_coordinates&format=json';

if (Meteor.isServer) {
  Meteor.publish('standortes', function() {
    return Standortes.find();
  });

  Meteor.publish('standorte', function(name) {
    return Standortes.find({ name });
  });

  // fetch standortes from the erp php system, resets
  // standortes collection by removing existing standortes,
  // and saves the new fetched standortes
  // finally, returns a promise, which resolves to true if
  // everythin went well
  Meteor.methods({
    'standortes.sync'() {
      if (!this.userId) throw new Meteor.Error('not-authorized');
      this.unblock();

      return new Promise(resolve => {
        HTTP.post(
          STANDORTES_URL,
          {
            params: {
              username: 'service',
              password: '20Service17!',
            },
          },
          (error, result) => {
            const standortes = JSON.parse(result.content);
            Standortes.remove({});
            standortes.forEach(standorte => Standortes.insert(standorte));
            resolve(true);
          }
        );
      });
    },

    // receives searchTerm from standortes page, and looks for any
    // coincidence of searchTerm inside the standorte record name
    // finally, returns all found standortes
    'standortes.search'(searchTerm) {
      if (searchTerm === '') {
        throw new Meteor.Error(400, 'Please enter a search term.');
      }

      const re = new RegExp(`${searchTerm}`, 'i');
      const foundStandortes = Standortes.find({ name: { $in: [re] } }).fetch();
      return foundStandortes;
    },
  });
}
