import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const Status = new Mongo.Collection('status');

if (Meteor.isServer) {
	Meteor.publish('status', function() {
		return Status.find();
	});
}

Meteor.methods({
	'update.deployStatus'(deployStatus) {
		new SimpleSchema({ deployStatus: { type: string } }).validate({
			deployStatus,
		});
		const selector = { type: 'deploy' };
		const modifier = { status: deployStatus };
		Status.upsert(selector, { $set: modifier });
	},
});
