import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';

if (Meteor.isServer) {
  Meteor.publish('users', function() {
    return Meteor.users.find();
  });
}

Meteor.methods({
  'users.create'(username, password) {
    if (!Roles.userIsInRole(this.userId, 'super-admin')) {
      throw new Meteor.Error(403, 'Not authorized to create new users');
    }

    return new Promise((resolve, reject) => {
      try {
        const userId = Accounts.createUser({ username, password });
        Roles.addUsersToRoles(userId, ['admin']);
        resolve(true);
      } catch (err) {
        reject(err);
      }
    });
  },

  'users.delete'(_id) {
    if (!this.userId || !Roles.userIsInRole(this.userId, 'super-admin')) {
      throw new Meteor.Error(403, 'Not authorized to delete users');
    }

    Meteor.users.remove({ _id });
  },
});
