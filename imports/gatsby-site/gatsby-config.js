module.exports = {
  siteMetadata: {
    title: 'update Fitness',
  },
  plugins: [
    {
      resolve: 'gatsby-source-mongodb',
      options: {
        dbName: 'update_fitness_dev',
        collection: ['images', 'news', 'pages', 'standortes'],
        server: {
          address: 'ds127961.mlab.com',
          port: 27961,
        },
        auth: {
          user: process.env.MLAB_USER || 'victor13',
          password: process.env.MLAB_PASSWORD || 'victor13',
        },
      },
    },
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-styled-components',
  ],
  proxy: {
    prefix: '/api',
    url: 'http://localhost:3000',
  },
}
