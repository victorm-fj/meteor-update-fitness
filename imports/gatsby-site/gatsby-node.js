const path = require('path')

exports.createPages = async ({ actions, graphql }) => {
  const { createPage } = actions
  const standorteTemplate = path.resolve(
    'src/templates/standorte-page/StandortePageContainer.js'
  )
  const { data } = await graphql(`
    {
      allMongodbUpdateFitnessDevStandortes {
        edges {
          node {
            id
            state
            address1
            zip
            name
            city
            lat
            lon
          }
        }
      }
      allMongodbUpdateFitnessDevImages {
        edges {
          node {
            id
            component
            page
            updatedAt
            imageUrl
            sliders {
              imageUrl
            }
          }
        }
      }
      allMongodbUpdateFitnessDevNews {
        edges {
          node {
            id
            location
            title
            body
            order
            updatedAt
          }
        }
      }
      allMongodbUpdateFitnessDevPages {
        edges {
          node {
            id
            page
            component
            htmlContent
          }
        }
      }
    }
  `)
  for (const { node } of data.allMongodbUpdateFitnessDevStandortes.edges) {
    createPage({
      path: `/standorte/${node.name.split(' ').join('-')}`,
      component: standorteTemplate,
      context: { id: node.id, name: node.name },
    })
  }
}
