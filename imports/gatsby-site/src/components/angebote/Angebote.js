import React from 'react'
import PropTypes from 'prop-types'

import OryRenderer from '../oryRenderer/OryRenderer'
import EditableText from '../editableText/EditableText'
import StyledAngebote from './StyledAngebote'

const Angebote = props => (
  <StyledAngebote>
    <OryRenderer oryEditorContent={props.oryEditorContent} />
    <div style={{ width: '100%' }}>
      <EditableText editableContent={props.editableContent} />
    </div>
  </StyledAngebote>
)

Angebote.propTypes = {
  editableContent: PropTypes.string.isRequired,
  oryEditorContent: PropTypes.object.isRequired,
}
Angebote.defaultProps = {
  editableContent: '',
  oryEditorContent: {},
}

export default Angebote
