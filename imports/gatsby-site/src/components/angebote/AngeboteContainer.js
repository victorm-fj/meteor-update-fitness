import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

import Angebote from './Angebote'

const AngeboteContainer = ({ data }) => (
  <StaticQuery
    query={graphql`
      query AngeboteQuery {
        allMongodbUpdateFitnessDevPages(filter: { page: { eq: "angebote" } }) {
          edges {
            node {
              id
              page
              component
              htmlContent
              content {
                id
                cells {
                  id
                  size
                  rows {
                    id
                    cells {
                      id
                      inline
                      size
                      content {
                        plugin {
                          name
                          version
                        }
                        state {
                          src
                          serialized {
                            nodes {
                              kind
                              type
                              nodes {
                                kind
                                text
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    `}
    render={data => {
      const { allMongodbUpdateFitnessDevPages } = data
      let editableContent = ''
      let oryEditorContent = {}
      if (allMongodbUpdateFitnessDevPages) {
        allMongodbUpdateFitnessDevPages.edges.forEach(({ node }) => {
          if (node.htmlContent) {
            editableContent = node.htmlContent
          }
          if (node.content) {
            oryEditorContent = node.content
          }
        })
      }
      return (
        <Angebote
          editableContent={editableContent}
          oryEditorContent={oryEditorContent}
        />
      )
    }}
  />
)

export default AngeboteContainer
