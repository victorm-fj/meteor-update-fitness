import styled from 'styled-components'

const StyledAngebote = styled.div`
  width: 80%;
  height: 100%;
  margin: 8rem auto;
  iframe {
    width: 100%;
    height: 100vh;
    border: none;
  }
`

export default StyledAngebote
