import React from 'react'
import PropTypes from 'prop-types'

import StyledEditableImage from './StyledEditableImage'

const EditableImage = props => (
  <StyledEditableImage>
    <img src={props.image} alt={props.alt} />
  </StyledEditableImage>
)

EditableImage.defaultProps = {
  alt: '',
  containerStyle: { position: 'absolute', right: '-15%', top: '-10%' },
}

EditableImage.propTypes = {
  image: PropTypes.string.isRequired,
}

export default EditableImage
