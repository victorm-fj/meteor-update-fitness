import React from 'react'
import PropTypes from 'prop-types'

import StyledEditableText from './StyledEditableText'

const EditableText = props => (
  <StyledEditableText>
    <div
      style={{ width: '100%' }}
      dangerouslySetInnerHTML={{
        __html: props.editableContent,
      }}
    />
  </StyledEditableText>
)

EditableText.defaultProps = {
  containerStyle: { position: 'absolute', right: '-2.2rem', top: '-2rem' },
}

EditableText.propTypes = {
  editableContent: PropTypes.string.isRequired,
}

export default EditableText
