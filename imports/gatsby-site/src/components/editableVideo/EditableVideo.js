import React from 'react'
import PropTypes from 'prop-types'

import StyledEditableVideo from './StyledEditableVideo'

const EditableVideo = props => (
  <StyledEditableVideo>
    <iframe
      title="Probetraining im update Fitness"
      width="100%"
      height="250"
      src={props.editableContent}
      frameBorder="0"
      allowFullScreen
    />
  </StyledEditableVideo>
)

EditableVideo.propTypes = {
  editableContent: PropTypes.string.isRequired,
}

export default EditableVideo
