import React from 'react'
import { Link } from 'gatsby'

import StyledFooter from './StyledFooter'
import StyledContent from './StyledContent'

const Footer = () => {
  return (
    <StyledFooter>
      <StyledContent>
        <div>
          <h4>UNTERNEHMEN</h4>
          <Link to="/">Werte</Link>
          <Link to="/">Geschichte</Link>
          <Link to="/">Partner</Link>
          <Link to="/">Expansion</Link>
        </div>
        <div>
          <h4>JOBS </h4>
          <Link to="/">Ausbildung </Link>
          <Link to="/">Offene Stellen</Link>
        </div>
        <div>
          <h4>KONTAKT</h4>
          <Link to="/">Stundenpläne</Link>
          <Link to="/">Standorte</Link>
        </div>
      </StyledContent>

      <p>(c) update Fitness 2015</p>
    </StyledFooter>
  )
}

export default Footer
