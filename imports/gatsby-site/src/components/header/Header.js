import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

import SocialHeader from './social/SocialHeader'
import NavBarHeader from './navbar/NavBarHeader'

const Header = ({ data }) => (
  <StaticQuery
    query={graphql`
      query HeaderImagesQuery {
        allMongodbUpdateFitnessDevImages(
          filter: { page: { eq: "home" }, component: { eq: "navbar" } }
        ) {
          edges {
            node {
              id
              page
              component
              imageUrl
            }
          }
        }
      }
    `}
    render={data => (
      <>
        <SocialHeader />
        <NavBarHeader
          logo={data.allMongodbUpdateFitnessDevImages.edges[0].node}
        />
      </>
    )}
  />
)

export default Header
