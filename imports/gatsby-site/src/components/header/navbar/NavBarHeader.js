import React from 'react'
import { Link } from 'gatsby'
import { UnmountClosed } from 'react-collapse'
import PropTypes from 'prop-types'

import DesktopNav from './desktop/DesktopNav'
import MobileNav from './mobile/MobileNav'
import StyledHamburger from './StyledHamburger'
import StyledNavBar from './StyledNavBar'

class NavBarHeader extends React.Component {
  state = { open: false }
  onToggle = () => {
    this.setState({ open: !this.state.open })
  }
  render() {
    return (
      <StyledNavBar>
        <div style={{ flex: 1, alignSelf: 'flex-end' }}>
          <UnmountClosed isOpened={this.state.open} fixedHeight={230}>
            <MobileNav onToggle={this.onToggle} />
          </UnmountClosed>

          <DesktopNav />

          <StyledHamburger
            className={this.state.open ? 'open' : null}
            onClick={this.onToggle}
          />
        </div>
        <div style={{ textAlign: 'right' }}>
          <Link to="/">
            <img
              src={this.props.logo.imageUrl}
              className="logo"
              alt="update Fitness logo"
            />
          </Link>
        </div>
      </StyledNavBar>
    )
  }
}

NavBarHeader.propTypes = {
  logo: PropTypes.object.isRequired,
}

export default NavBarHeader
