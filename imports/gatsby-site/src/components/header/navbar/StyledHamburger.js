import styled from 'styled-components'

const StyledHamburger = styled.button`
  position: absolute;
  top: 45px;
  left: 50px;
  display: none;
  border: 0;
  background: none;
  outline: 0;
  padding: 0;
  cursor: pointer;

  border-bottom: 4px solid ${props => props.theme.brand};
  width: 24px;

  transition: border-bottom 1s ease-in-out;
  -webkit-transition: border-bottom 1s ease-in-out;

  ${'' /* Fix for extra space in Firefox */}&::-moz-focus-inner {
    border: 0;
    padding: 0;
  }

  &:before {
    content: '';
    display: block;
    border-bottom: 4px solid ${props => props.theme.brand};
    width: 100%;
    margin-bottom: 5px;
    transition: transform 0.5s ease-in-out;
    -webkit-transition: -webkit-transform 0.5s ease-in-out;
  }

  &:after {
    content: '';
    display: block;
    border-bottom: 4px solid ${props => props.theme.brand};
    width: 100%;
    margin-bottom: 5px;
    transition: transform 0.5s ease-in-out;
    -webkit-transition: -webkit-transform 0.5s ease-in-out;
  }

  &.open {
    border-bottom: 4px solid transparent;
    transition: border-bottom 0.8s ease-in-out;
    -webkit-transition: border-bottom 0.8s ease-in-out;

    &:before {
      transform: rotate(-405deg) translateY(1px) translateX(-3px);
      -webkit-transform: rotate(-405deg) translateY(1px) translateX(-3px);
      transition: transform 0.5s ease-in-out;
      -webkit-transition: -webkit-transform 0.5s ease-in-out;
    }

    &:after {
      transform: rotate(405deg) translateY(-4px) translateX(-5px);
      -webkit-transform: rotate(405deg) translateY(-4px) translateX(-5px);
      transition: transform 0.5s ease-in-out;
      -webkit-transition: -webkit-transform 0.5s ease-in-out;
    }
  }

  @media (max-width: 900px) {
    display: block;
  }
`

export default StyledHamburger
