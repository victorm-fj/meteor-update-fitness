import React from 'react'
import { Link } from 'gatsby'

import StyledDesktopNav from './StyledDesktopNav'

const DesktopNav = () => (
  <StyledDesktopNav>
    <li>
      <Link to="/standorte">STANDORTE</Link>
    </li>
    <li>
      <Link to="/angebote">ANGEBOTE</Link>
    </li>
    <li>
      <Link to="/probetraining">PROBETRAINING</Link>
    </li>
    <li>
      <Link to="/mitglied">MITGLIED WERDEN</Link>
    </li>
  </StyledDesktopNav>
)

export default DesktopNav
