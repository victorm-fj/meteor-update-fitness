import React from 'react'
import { Link } from 'gatsby'
import PropTypes from 'prop-types'

import StyledMobileNav from './StyledMobileNav'

const MobileNav = ({ onToggle }) => (
  <StyledMobileNav>
    <li>
      <Link to="/standorte" onClick={onToggle}>
        STANDORTE
      </Link>
    </li>

    <li>
      <Link to="/angebote" onClick={onToggle}>
        ANGEBOTE
      </Link>
    </li>

    <li>
      <Link to="/probetraining" onClick={onToggle}>
        PROBETRAINING
      </Link>
    </li>

    <li>
      <Link to="/mitglied" onClick={onToggle}>
        MITGLIED WERDEN
      </Link>
    </li>
  </StyledMobileNav>
)

MobileNav.propTypes = {
  onToggle: PropTypes.func.isRequired,
}

export default MobileNav
