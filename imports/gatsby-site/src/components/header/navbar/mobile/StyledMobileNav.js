import styled from 'styled-components'

const StyledMobileNav = styled.ul`
  padding: 120px 0 0;
  list-style: none;

  li {
    width: 100%;
    padding-right: 30px;
    padding-bottom: 10px;
    font-weight: 800;
    margin: 0;

    & > a {
      font-size: 1.6rem;
    }
  }
`

export default StyledMobileNav
