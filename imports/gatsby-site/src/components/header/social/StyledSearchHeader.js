import styled from 'styled-components'

const StyledSearchHeader = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  input[type='text']::placeholder {
    color: #fafafa;
    font-size: 1.4rem;
  }

  button {
    background: none;
    border: none;
    cursor: pointer;
    display: flex;
    justify-content: center;
    align-items: center;
  }
`

export default StyledSearchHeader
