import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

import Slider from '../slider/Slider'
import Probetraining from './probetraining/Probetraining'
import EServices from './eServices/EServices'
import UpdateErfolge from './updateErfolge/UpdateErfolge'
import Statistics from './statistics/Statistics'
import StyledHome from './StyledHome'

const Home = ({ data }) => (
  <StaticQuery
    query={graphql`
      query HomeSliderImagesNewsQuery {
        allMongodbUpdateFitnessDevImages(
          filter: { page: { eq: "home" }, component: { eq: "slider" } }
        ) {
          edges {
            node {
              id
              component
              page
              updatedAt
              sliders {
                imageUrl
              }
            }
          }
        }
        allMongodbUpdateFitnessDevNews(
          filter: { location: { eq: "home" } }
          sort: { fields: [order, updatedAt], order: DESC }
        ) {
          edges {
            node {
              id
              title
              body
              order
              updatedAt
            }
          }
        }
      }
    `}
    render={data => {
      const news = data.allMongodbUpdateFitnessDevNews.edges.map(
        edge => edge.node
      )
      const images = data.allMongodbUpdateFitnessDevImages.edges[0].node.sliders.map(
        (slider, index) => {
          if (news && news[index]) {
            return {
              imgSrc: slider.imageUrl,
              imgAlt: 'Slider image',
              newsTitle: news[index].title,
              newsBody: news[index].body,
            }
          } else {
            return {
              imgSrc: slider.imageUrl,
              imgAlt: 'Slider image',
              newsTitle: '',
              newsBody: '',
            }
          }
        }
      )
      return (
        <StyledHome>
          <Slider images={images} news={news} />
          <div className="news">
            <a href="/">alle News</a>
          </div>
          <div className="content">
            <Probetraining />
            <EServices />
            <UpdateErfolge />
            <Statistics />
          </div>
        </StyledHome>
      )
    }}
  />
)

export default Home
