import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

import EditableText from '../../editableText/EditableText'
import EditableImage from '../../editableImage/EditableImage'
import StyledEServices from './StyledEServices'

const EServices = ({ data }) => (
  <StaticQuery
    query={graphql`
      query EServicesPagesQuery {
        allMongodbUpdateFitnessDevPages(
          filter: { page: { eq: "home" }, component: { eq: "services" } }
        ) {
          edges {
            node {
              id
              component
              page
              htmlContent
              updatedAt
            }
          }
        }
        allMongodbUpdateFitnessDevImages(
          filter: { page: { eq: "home" }, component: { eq: "services" } }
        ) {
          edges {
            node {
              id
              component
              page
              imageUrl
              updatedAt
            }
          }
        }
      }
    `}
    render={data => (
      <section>
        <StyledEServices style={{ flexDirection: 'row' }}>
          <div style={{ flex: 0.4 }}>
            <EditableImage
              alt="E Services"
              image={
                data.allMongodbUpdateFitnessDevImages.edges[0].node.imageUrl
              }
            />
          </div>
          <div style={{ flex: 0.6, marginLeft: '1rem' }}>
            <EditableText
              editableContent={
                data.allMongodbUpdateFitnessDevPages.edges[0].node.htmlContent
              }
            />
          </div>
        </StyledEServices>
      </section>
    )}
  />
)

export default EServices
