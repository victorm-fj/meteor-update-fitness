import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

import StyledProbetraining from './StyledProbetraining'
import EditableText from '../../editableText/EditableText'
import EditableVideo from '../../editableVideo/EditableVideo'

const Probetraining = ({ data }) => (
  <StaticQuery
    query={graphql`
      query ProbetrainingPagesQuery {
        allMongodbUpdateFitnessDevPages(
          filter: { page: { eq: "home" }, component: { eq: "probetraining" } }
        ) {
          edges {
            node {
              id
              component
              page
              htmlContent
              updatedAt
              link
            }
          }
        }
      }
    `}
    render={data => (
      <section>
        <StyledProbetraining style={{ flexDirection: 'row' }}>
          <div style={{ flex: 0.35 }}>
            <EditableText
              editableContent={
                data.allMongodbUpdateFitnessDevPages.edges[0].node.htmlContent
              }
            />
            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                marginTop: '4rem',
              }}
            >
              <p>
                <a className="probetraining-link" href="/">
                  Online-Anmeldung zum kostenlosen und unverbindlichen
                  Probetraining
                </a>
              </p>
            </div>
          </div>
          <div style={{ flex: 0.15 }} />
          <div style={{ flex: 0.5 }}>
            <EditableVideo
              editableContent={
                data.allMongodbUpdateFitnessDevPages.edges[0].node.link
              }
            />
          </div>
        </StyledProbetraining>
      </section>
    )}
  />
)

export default Probetraining
