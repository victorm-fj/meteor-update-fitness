import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

import EditableText from '../../editableText/EditableText'
import EditableImage from '../../editableImage/EditableImage'
import StyledStatistics from './StyledStatistics'

const Statistics = ({ data }) => (
  <StaticQuery
    query={graphql`
      query StatisticsPagesQuery {
        allMongodbUpdateFitnessDevPages(
          filter: { page: { eq: "home" }, component: { eq: "statistics" } }
        ) {
          edges {
            node {
              id
              component
              page
              htmlContent
              updatedAt
            }
          }
        }
        allMongodbUpdateFitnessDevImages(
          filter: { page: { eq: "home" }, component: { eq: "statistics" } }
        ) {
          edges {
            node {
              id
              component
              page
              imageUrl
              updatedAt
            }
          }
        }
      }
    `}
    render={data => (
      <section>
        <StyledStatistics style={{ flexDirection: 'row' }}>
          <div style={{ flex: 0.5 }}>
            <EditableText
              editableContent={
                data.allMongodbUpdateFitnessDevPages.edges[0].node.htmlContent
              }
            />
          </div>

          <div style={{ flex: 0.1 }} />

          <div style={{ flex: 0.4 }}>
            <EditableImage
              alt="Training statistics"
              image={
                data.allMongodbUpdateFitnessDevImages.edges[0].node.imageUrl
              }
            />
          </div>
        </StyledStatistics>
      </section>
    )}
  />
)

export default Statistics
