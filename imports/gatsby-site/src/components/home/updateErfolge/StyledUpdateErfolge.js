import styled from 'styled-components';

const StyledUpdateErfolge = styled.div`
  margin: 4rem auto;
  background: #edf3f7;
  box-shadow: 0 0 0.5rem ${props => props.theme.boxShadow};
  padding: 4rem;
  display: flex;

  & > div:first-child {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    img {
      width: 22rem;
      height: 25rem;
    }
  }

  .success-link {
    display: flex;
    align-items: center;
  }

  .success-link:before {
    content: '>';
    color: ${props => props.theme.brand};
    font-weight: 800;
    font-size: 2.4rem;
    margin-right: 0.5rem;
  }

  @media (max-width: 800px) {
    flex-direction: column;

    & > div:first-child {
      display: flex;
      justify-content: center;
      align-items: center;

      img {
        padding-bottom: 4rem;
      }
    }
  }
`;

export default StyledUpdateErfolge;
