import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

import EditableText from '../../editableText/EditableText'
import EditableImage from '../../editableImage/EditableImage'
import StyledUpdateErfolge from './StyledUpdateErfolge'

const UpdateErfolge = ({ data }) => (
  <StaticQuery
    query={graphql`
      query UpdateErfolgePagesQuery {
        allMongodbUpdateFitnessDevPages(
          filter: { page: { eq: "home" }, component: { eq: "update" } }
        ) {
          edges {
            node {
              id
              component
              page
              htmlContent
              updatedAt
            }
          }
        }
        allMongodbUpdateFitnessDevImages(
          filter: { page: { eq: "home" }, component: { eq: "update" } }
        ) {
          edges {
            node {
              id
              component
              page
              imageUrl
              updatedAt
            }
          }
        }
      }
    `}
    render={data => (
      <section>
        <StyledUpdateErfolge style={{ flexDirection: 'row' }}>
          <div style={{ flex: 0.4 }}>
            <EditableImage
              alt="Update Erfolge"
              image={
                data.allMongodbUpdateFitnessDevImages.edges[0].node.imageUrl
              }
            />
          </div>
          <div style={{ flex: 0.1 }} />
          <div style={{ flex: 0.5 }}>
            <EditableText
              editableContent={
                data.allMongodbUpdateFitnessDevPages.edges[0].node.htmlContent
              }
            />

            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                marginTop: '4rem',
              }}
            >
              <p>
                <a className="success-link" href="/">
                  Poste uns deinen persönlichen Erfolg!
                </a>
              </p>
            </div>
          </div>
        </StyledUpdateErfolge>
      </section>
    )}
  />
)

export default UpdateErfolge
