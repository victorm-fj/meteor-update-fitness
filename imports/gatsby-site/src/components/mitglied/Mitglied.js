import React from 'react'
import PropTypes from 'prop-types'

import StyledMitglied from './StyledMitglied'
import OryRenderer from '../oryRenderer/OryRenderer'

const Mitglied = props => (
  <StyledMitglied>
    <OryRenderer oryEditorContent={props.oryEditorContent} />
  </StyledMitglied>
)

Mitglied.propTypes = {
  oryEditorContent: PropTypes.object.isRequired,
}
Mitglied.defaultProps = {
  oryEditorContent: {},
}

export default Mitglied
