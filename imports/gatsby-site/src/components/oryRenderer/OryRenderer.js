import React from 'react'
import PropTypes from 'prop-types'
import { HTMLRenderer } from 'ory-editor-renderer'
// The rich text area plugin
import slate from 'ory-editor-plugins-slate'
import 'ory-editor-plugins-slate/lib/index.css'
// The spacer plugin
import spacer from 'ory-editor-plugins-spacer'
import 'ory-editor-plugins-spacer/lib/index.css'
// The image plugin
import image from 'ory-editor-plugins-image'
import 'ory-editor-plugins-image/lib/index.css'
// The video plugin
import video from 'ory-editor-plugins-video'
import 'ory-editor-plugins-video/lib/index.css'
// The native handler plugin
import native from 'ory-editor-plugins-default-native'
// The divider plugin
import divider from 'ory-editor-plugins-divider'

import StyledOryRenderer from './StyledOryRenderer'

// Define which plugins we want to use. We only have slate and parallax available, so load those.
const plugins = {
  content: [slate(), spacer, image, video, divider],
  layout: [
    /* parallax({ defaultPlugin: slate() }) */
  ],
  // If you pass the native key the editor will be able to handle native drag and drop events (such as links, text, etc).
  // The native plugin will then be responsible to properly display the data which was dropped onto the editor.
  native,
}

const OryRenderer = props => (
  <StyledOryRenderer>
    <HTMLRenderer state={props.oryEditorContent} plugins={plugins} />
  </StyledOryRenderer>
)

OryRenderer.propTypes = {
  oryEditorContent: PropTypes.object.isRequired,
}
OryRenderer.defaultPropt = {
  oryEditorContent: {},
}

export default OryRenderer
