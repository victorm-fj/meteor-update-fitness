import React from 'react'
import PropTypes from 'prop-types'

import StyledProbetraining from './StyledProbetraining'
import OryRenderer from '../oryRenderer/OryRenderer'

const Probetraining = props => (
  <StyledProbetraining>
    <OryRenderer oryEditorContent={props.oryEditorContent} />
  </StyledProbetraining>
)

Probetraining.propTypes = {
  oryEditorContent: PropTypes.object.isRequired,
}
Probetraining.defaultProps = {
  oryEditorContent: {},
}

export default Probetraining
