import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

import Probetraining from './Probetraining'

const ProbetrainingContainer = ({ data }) => (
  <StaticQuery
    query={graphql`
      query ProbetrainingQuery {
        allMongodbUpdateFitnessDevPages(
          filter: { page: { eq: "probetraining" } }
        ) {
          edges {
            node {
              id
              page
              component
              htmlContent
              content {
                id
                cells {
                  id
                  size
                  rows {
                    id
                    cells {
                      id
                      inline
                      size
                      content {
                        plugin {
                          name
                          version
                        }
                        state {
                          src
                          serialized {
                            nodes {
                              kind
                              type
                              nodes {
                                kind
                                text
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    `}
    render={data => {
      const { allMongodbUpdateFitnessDevPages } = data
      let editableContent = ''
      let oryEditorContent = {}
      if (allMongodbUpdateFitnessDevPages) {
        allMongodbUpdateFitnessDevPages.edges.forEach(({ node }) => {
          if (node.htmlContent) {
            editableContent = node.htmlContent
          }
          if (node.content) {
            oryEditorContent = node.content
          }
        })
      }
      return <Probetraining oryEditorContent={oryEditorContent} />
    }}
  />
)

export default ProbetrainingContainer
