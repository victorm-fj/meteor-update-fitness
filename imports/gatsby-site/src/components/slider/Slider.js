import React from 'react'
import ImageGallery from 'react-image-gallery'
import PropTypes from 'prop-types'
import 'react-image-gallery/styles/css/image-gallery.css'

import StyledSlider from './StyledSlider'
import Item from './item/Item'

class Slider extends React.Component {
  pause = () => {
    this.gallery.pause()
  }
  play = () => {
    this.gallery.play()
  }
  renderItem = item => <Item {...item} />
  render() {
    return (
      <StyledSlider onMouseEnter={this.pause} onMouseLeave={this.play}>
        <ImageGallery
          ref={gallery => (this.gallery = gallery)}
          items={this.props.images}
          // slideInterval={3000}
          onImageLoad={this.handleImageLoad}
          showThumbnails={false}
          showFullscreenButton={false}
          showPlayButton={false}
          showBullets
          // autoPlay={false}
          renderItem={this.renderItem}
        />
      </StyledSlider>
    )
  }
}

Slider.propTypes = {
  images: PropTypes.array.isRequired,
}
export default Slider
