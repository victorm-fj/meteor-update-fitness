import React from 'react'

import StyledItem from './StyledItem'

const Item = ({ imgSrc, imgAlt, newsTitle, newsBody }) => (
  <StyledItem className="image-gallery-image">
    <img src={imgSrc} alt={imgAlt} />
    {newsTitle && (
      <div className="news">
        <div className="news-title">
          <p>
            <strong>{newsTitle}</strong>
          </p>
        </div>
        <div className="news-body">
          <div className="news-body-content">
            <p>{newsBody}</p>
          </div>
        </div>
      </div>
    )}
  </StyledItem>
)

export default Item
