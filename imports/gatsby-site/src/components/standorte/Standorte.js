import React from 'react'
import PropTypes from 'prop-types'

import MapSection from './map/MapSection'
import SearchResults from './searchResults/SearchResults'
import AlleStandorte from './alleStandorte/AlleStandorte'
import StyledStandorte from './StyledStandorte'

class Standorte extends React.Component {
  state = {
    searchTerm: '',
    searchResults: [],
    isLoading: false,
    coords: {},
    locations: this.props.locations,
  }
  componentDidMount() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition(
        position => {
          const { latitude, longitude } = position.coords
          this.setState({ coords: { latitude, longitude } })
        },
        () => {
          alert('Position could not be determined.')
        },
        {
          enableHighAccuracy: true,
        }
      )
    }
  }
  onChangeSearchTerm = e => {
    const searchTerm = e.target.value
    this.setState({ searchTerm })
  }
  onSubmit = e => {
    e.preventDefault()
    const { searchTerm, locations } = this.state
    if (searchTerm === '') {
      alert('Please enter a search term.')
    } else {
      this.setState({ isLoading: true })
      const regex = new RegExp(searchTerm.trim(), 'i')
      const results = locations.filter(location => regex.test(location.name))
      if (results.length === 0) {
        this.setState({
          searchResults: [undefined],
          isLoading: false,
        })
      } else {
        this.setState({
          searchResults: results,
          isLoading: false,
        })
      }
    }
  }
  render() {
    const { searchResults, coords } = this.state
    const userLocation = {
      lat: coords.latitude,
      lon: coords.longitude,
      userLoc: true,
    }
    const locations = [...this.props.locations, userLocation]
    return (
      <div>
        <MapSection
          locations={locations}
          searchTerm={this.state.searchTerm}
          onChange={this.onChangeSearchTerm}
          onSubmit={this.onSubmit}
          searchPlace={this.props.searchPlace}
        />
        <StyledStandorte>
          {searchResults.length > 0 && (
            <SearchResults coords={coords} results={searchResults} />
          )}
          <AlleStandorte
            locations={this.props.locations}
            allstandorte={this.props.allstandorte}
          />
        </StyledStandorte>
      </div>
    )
  }
}

Standorte.propTypes = {
  locations: PropTypes.array.isRequired,
  allstandorte: PropTypes.string.isRequired,
  searchPlace: PropTypes.string.isRequired,
}
export default Standorte
