import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

import Standorte from './Standorte'

const StandorteContainer = ({ data }) => (
  <StaticQuery
    query={graphql`
      query StandorteStandortesQuery {
        allMongodbUpdateFitnessDevStandortes(
          sort: { fields: [name], order: ASC }
        ) {
          edges {
            node {
              id
              state
              address1
              zip
              name
              city
              lat
              lon
            }
          }
        }
        allMongodbUpdateFitnessDevPages(filter: { page: { eq: "standorte" } }) {
          edges {
            node {
              id
              htmlContent
            }
          }
        }
      }
    `}
    render={data => (
      <Standorte
        locations={data.allMongodbUpdateFitnessDevStandortes.edges.map(
          edge => edge.node
        )}
        allstandorte={
          data.allMongodbUpdateFitnessDevPages.edges[0].node.htmlContent
        }
        searchPlace={
          data.allMongodbUpdateFitnessDevPages.edges[1].node.htmlContent
        }
      />
    )}
  />
)

export default StandorteContainer
