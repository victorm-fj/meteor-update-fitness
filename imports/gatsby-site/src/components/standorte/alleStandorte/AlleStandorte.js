import React from 'react'
import { Link } from 'gatsby'
import PropTypes from 'prop-types'

import EditableText from '../../editableText/EditableText'
import StyledAlleStandorte from './StyledAlleStandorte'

const AlleStandorte = props => (
  <section>
    <div style={{ position: 'relative' }}>
      <EditableText editableContent={props.allstandorte} />
    </div>
    <StyledAlleStandorte>
      <ul>
        {props.locations
          .filter(location => location.lat !== null)
          .map(location => (
            <li key={location.id}>
              <Link to={`/standorte/${location.name.split(' ').join('-')}`}>
                {location.name}
              </Link>
            </li>
          ))}
      </ul>
    </StyledAlleStandorte>
  </section>
)

AlleStandorte.propTypes = {
  locations: PropTypes.array.isRequired,
  allstandorte: PropTypes.string.isRequired,
}

export default AlleStandorte
