import React from 'react'
import PropTypes from 'prop-types'

import Map from './Map'
import SearchPlace from './SearchPlace'
import StyledMapSection from './StyledMapSection'

const MapSection = ({
  locations,
  searchTerm,
  onChange,
  onSubmit,
  searchPlace,
}) => (
  <StyledMapSection>
    <SearchPlace
      searchTerm={searchTerm}
      onChange={onChange}
      onSubmit={onSubmit}
      searchPlace={searchPlace}
    />
    <div>
      <Map locations={locations} />
    </div>
  </StyledMapSection>
)

MapSection.propTypes = {
  locations: PropTypes.array.isRequired,
  searchTerm: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  searchPlace: PropTypes.string.isRequired,
}

export default MapSection
