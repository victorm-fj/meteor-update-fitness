import React from 'react'
import PropTypes from 'prop-types'
import FaSearch from 'react-icons/lib/fa/search'

import EditableText from '../../editableText/EditableText'
import StyledSearchPlace from './StyledSearchPlace'

const SearchPlace = props => (
  <StyledSearchPlace>
    <div style={{ position: 'relative' }}>
      <EditableText editableContent={props.searchPlace} />
    </div>

    <form onSubmit={props.onSubmit}>
      <input
        type="text"
        style={{
          background: '#777',
          border: 'none',
          outline: 'none',
          color: '#fafafa',
          padding: '10px',
          marginRight: '10px',
        }}
        value={props.searchTerm}
        onChange={props.onChange}
      />

      <button type="submit">
        <FaSearch size={24} color="#fafafa" />
      </button>
    </form>
  </StyledSearchPlace>
)

SearchPlace.propTypes = {
  searchTerm: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  searchPlace: PropTypes.string.isRequired,
}

export default SearchPlace
