import styled from 'styled-components';

const StyledMapSection = styled.div`
  background: ${props => props.theme.darkBackground};
  padding: 2.2rem 5%;
  display: flex;

  & > div {
    flex: 0.6;
  }

  & > div:first-child {
    flex: 0.4;
  }

  @media (max-width: 900px) {
    flex-direction: column;

    & > div {
      order: 1;
    }

    & > div:first-child {
      order: 2;
    }
  }
`;

export default StyledMapSection;
