import React from 'react';

import PropTypes from 'prop-types';

import Success from './Success';
import Failure from './Failure';
import StyledSearchResults from './StyledSearchResults';

const SearchResult = ({ results, coords }) => (
  <section>
    <StyledSearchResults>
      {typeof results[0] === 'undefined' ? (
        <Failure />
      ) : (
        <Success coords={coords} results={results} />
      )}
    </StyledSearchResults>
  </section>
);

SearchResult.propTypes = {
  results: PropTypes.array,
  coords: PropTypes.shape({
    latitude: PropTypes.number,
    longitude: PropTypes.number,
  }),
};

export default SearchResult;
