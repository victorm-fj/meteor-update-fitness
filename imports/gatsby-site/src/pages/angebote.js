import React from 'react'

import Layout from '../components/layout/Layout'
import Angebote from '../components/angebote/AngeboteContainer'

const AngebotePage = () => (
  <Layout>
    <Angebote />
  </Layout>
)

export default AngebotePage
