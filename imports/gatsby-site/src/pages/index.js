import React from 'react'

import Layout from '../components/layout/Layout'
import Home from '../components/home/Home'

const IndexPage = () => (
  <Layout>
    <Home />
  </Layout>
)

export default IndexPage
