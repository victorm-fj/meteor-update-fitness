import React from 'react'

import Layout from '../components/layout/Layout'
import Mitglied from '../components/mitglied/MitgliedContainer'

const MitgliedPage = () => (
  <Layout>
    <Mitglied />
  </Layout>
)

export default MitgliedPage
