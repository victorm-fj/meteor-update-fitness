import React from 'react'

import Layout from '../components/layout/Layout'
import Probetraining from '../components/probetraining/ProbetrainingContainer'

const ProbetrainingPage = () => (
  <Layout>
    <Probetraining />
  </Layout>
)

export default ProbetrainingPage
