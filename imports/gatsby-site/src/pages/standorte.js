import React from 'react'

import Layout from '../components/layout/Layout'
import Standorte from '../components/standorte/StandorteContainer'

const StandortePage = () => (
  <Layout>
    <Standorte />
  </Layout>
)

export default StandortePage
