import React from 'react'

import Layout from '../../components/layout/Layout'
import Slider from '../../components/slider/Slider'
import EServices from './eServices/EServices'
import Contact from './contact/Contact'
import CoursesSection from './courses/Courses'
import Fitness from './fitness/Fitness'
import Schedule from './schedule/Schedule'
import StyledStandortePage from './StyledStandortePage'

class StandortePage extends React.Component {
  render() {
    console.log(this.props)
    const {
      data: {
        allMongodbUpdateFitnessDevNews,
        allMongodbUpdateFitnessDevImages,
        allMongodbUpdateFitnessDevPages,
        mongodbUpdateFitnessDevStandortes,
        mongodbUpdateFitnessDevImages,
      },
    } = this.props
    let eServicesHTMLContent = ''
    let scheduleHTMLContent = ''
    let fitnessHTMLContent = ''
    let contactHTMLContent = ''
    let coursesHTMLContent = ''
    if (allMongodbUpdateFitnessDevPages) {
      allMongodbUpdateFitnessDevPages.edges.forEach(({ node }) => {
        switch (node.component) {
          case 'services':
            eServicesHTMLContent = node.htmlContent
            break
          case 'schedule':
            scheduleHTMLContent = node.htmlContent
            break
          case 'fitness':
            fitnessHTMLContent = node.htmlContent
            break
          case 'contact':
            contactHTMLContent = node.htmlContent
            break
          case 'courses':
            coursesHTMLContent = node.htmlContent
            break
          default:
            break
        }
      })
    }
    const news = allMongodbUpdateFitnessDevNews
      ? allMongodbUpdateFitnessDevNews.edges.map(edge => edge.node)
      : []
    const sliderImages = allMongodbUpdateFitnessDevImages
      ? allMongodbUpdateFitnessDevImages.edges.filter(
          edge => edge.node.component === 'slider'
        )[0]
      : null
    let images = []
    if (sliderImages) {
      images = sliderImages.node.sliders.map((slider, index) => {
        if (news && news[index]) {
          return {
            imgSrc: slider.imageUrl,
            imgAlt: 'Slider image',
            newsTitle: news[index].title,
            newsBody: news[index].body,
          }
        } else {
          return {
            imgSrc: slider.imageUrl,
            imgAlt: 'Slider image',
            newsTitle: '',
            newsBody: '',
          }
        }
      })
    }
    let fitnessImg = ''
    if (allMongodbUpdateFitnessDevImages) {
      const fitnessImageArr = allMongodbUpdateFitnessDevImages.edges.filter(
        ({ node }) => node.component === 'fitness'
      )
      fitnessImg =
        fitnessImageArr.length > 0 ? fitnessImageArr[0].node.imageUrl : ''
    }

    return (
      <Layout>
        <div style={{ position: 'relative' }}>
          <Slider images={images} news={news} />
        </div>
        <StyledStandortePage>
          <EServices
            editableContent={eServicesHTMLContent}
            image={mongodbUpdateFitnessDevImages.imageUrl}
          />
          <Contact
            standorte={mongodbUpdateFitnessDevStandortes}
            editableContent={contactHTMLContent}
          />
          <CoursesSection
            page={mongodbUpdateFitnessDevStandortes.name}
            editableContent={coursesHTMLContent}
          />
          <Fitness editableContent={fitnessHTMLContent} image={fitnessImg} />
          <Schedule editableContent={scheduleHTMLContent} />
        </StyledStandortePage>
      </Layout>
    )
  }
}

export default StandortePage
