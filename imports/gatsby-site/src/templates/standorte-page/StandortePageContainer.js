import React from 'react'
import { graphql } from 'gatsby'

import StandortePage from './StandortePage'

const StandortePageContainer = props => <StandortePage {...props} />

export default StandortePageContainer

export const pageQuery = graphql`
  query StandorteById($id: String!, $name: String!) {
    mongodbUpdateFitnessDevStandortes(id: { eq: $id }) {
      id
      state
      address1
      zip
      name
      city
      lat
      lon
    }
    allMongodbUpdateFitnessDevImages(filter: { page: { eq: $name } }) {
      edges {
        node {
          id
          page
          component
          sliders {
            imageUrl
          }
          imageUrl
        }
      }
    }
    allMongodbUpdateFitnessDevNews(
      filter: { location: { eq: $name } }
      sort: { fields: [order, updatedAt], order: DESC }
    ) {
      edges {
        node {
          id
          title
          body
          order
          updatedAt
        }
      }
    }
    allMongodbUpdateFitnessDevPages(filter: { page: { eq: $name } }) {
      edges {
        node {
          id
          page
          component
          htmlContent
        }
      }
    }
    mongodbUpdateFitnessDevImages(component: { eq: "services" }) {
      id
      component
      page
      imageUrl
    }
  }
`
