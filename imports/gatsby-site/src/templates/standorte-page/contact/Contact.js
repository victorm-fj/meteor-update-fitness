import React from 'react'
import PropTypes from 'prop-types'

import EditableText from '../../../components/editableText/EditableText'
import StandorteMap from './StandorteMap'
import StyledContact from './StyledContact'

const Contact = props => {
  const { standorte, editableContent } = props
  if (standorte) {
    return (
      <section>
        <StyledContact>
          <div style={{ flex: 0.4 }}>
            <div style={{ position: 'relative' }}>
              <EditableText editableContent={editableContent} />
            </div>
            <p>update Fitness {standorte.name}</p>
            <p>{standorte.address1}</p>
            <p>
              {standorte.zip} {standorte.city}
            </p>
            <p>Centerleitung: Severin Häring</p>
            <h3>Telefon +41 71 244 83 47</h3>
          </div>

          <div style={{ flex: 0.6 }}>
            <StandorteMap {...standorte} />
          </div>
        </StyledContact>
      </section>
    )
  }

  return null
}

Contact.propTypes = {
  standorte: PropTypes.object,
  editableContent: PropTypes.string.isRequired,
}
Contact.defaultProps = {
  editableContent: '',
}

export default Contact
