import styled from 'styled-components';

const StyledContact = styled.div`
  margin: 4rem auto;
  background: ${props => props.theme.lightBlue};
  box-shadow: 0 0 0.5rem ${props => props.theme.boxShadow};
  padding: 4rem;
  display: flex;

  @media (max-width: 800px) {
    flex-direction: column;
  }
`;

export default StyledContact;
