import React from 'react'
import PropTypes from 'prop-types'

import EditableText from '../../../components/editableText/EditableText'
import StyledCourses from './StyledCourses'
import CoursesModal from './modal/CoursesModal'

class Courses extends React.Component {
  state = { isOpen: false, courses: [], loading: true }
  async componentDidMount() {
    const { page } = this.props
    let url = '/api/courses/'
    if (process.env.NODE_ENV === 'production') {
      url = 'https://update-fitness.now.sh/api/courses/'
    }
    url = `${url}${page.split(' ').join('-')}`
    console.log('fetch url', url)
    try {
      const response = await fetch(url)
      const courses = await response.json()
      this.setState({ courses, loading: false })
    } catch (error) {
      console.log('Error fetching courses', error)
      // this.setState({ loading: false })
    }
  }
  toggleModal = () => {
    this.setState({ isOpen: !this.state.isOpen })
  }
  render() {
    return (
      <section>
        <div style={{ position: 'relative' }}>
          <EditableText editableContent={this.props.editableContent} />
        </div>
        <StyledCourses>
          <div style={{ flex: 0.7 }}>
            <div className="course">
              <p>Donnerstag, 5. November 2015 - 18:00</p>
              <p>BBP (Bauch, Beine, Po) / Bodytone Dauer: 55min</p>
            </div>
            <div className="course">
              <p>Donnerstag, 5. November 2015 - 19:00</p>
              <p>P.I.I.T Dauer: 30min</p>
            </div>
            <div className="course">
              <p>Montag, 9. November 2015 - 12:15</p>
              <p>P.I.I.T Dauer: 30min</p>
            </div>
            <div className="course">
              <p>Montag, 9. November 2015 - 18:00</p>
              <p>Tae Bo Dauer: 55min</p>
            </div>
          </div>
          <div style={{ flex: 0.3 }}>
            <button onClick={this.toggleModal}>
              Stundenplan {this.props.page}
            </button>
            <CoursesModal
              isOpen={this.state.isOpen}
              toggleModal={this.toggleModal}
              courses={this.state.courses}
              page={this.props.page}
              loading={this.state.loading}
            />
          </div>
        </StyledCourses>
      </section>
    )
  }
}

Courses.propTypes = {
  page: PropTypes.string.isRequired,
  editableContent: PropTypes.string.isRequired,
}
Courses.defaulProps = {
  page: '',
  editableContent: '',
}

export default Courses
