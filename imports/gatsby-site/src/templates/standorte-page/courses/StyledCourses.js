import styled from 'styled-components';

const StyledCourses = styled.div`
  display: flex;

  & > div:last-child {
    display: flex;
    justify-content: center;
    align-items: center;

    button {
      font-size: 1.8rem;
      padding: 0.7rem;
      margin-right: 1rem;
      border: none;
      cursor: pointer;
      outline: none;
      background: none;
    }

    button:before {
      content: '>';
      color: ${props => props.theme.brand};
      font-weight: 800;
      font-size: 2.6rem;
      margin-right: 0.5rem;
    }
  }

  .course {
    display: flex;

    & > p {
      flex: 0.5;
    }

    @media (max-width: 800px) {
      flex-direction: column;
      margin-bottom: 1.4rem;
    }
  }

  @media (max-width: 1200px) {
    flex-direction: column;

    & > div:last-child {
      margin-top: 4rem;
    }
  }
`;

export default StyledCourses;
