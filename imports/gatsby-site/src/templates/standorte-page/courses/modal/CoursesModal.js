import React from 'react'
import Modal from 'react-modal'
import PropTypes from 'prop-types'
import FaDownload from 'react-icons/lib/fa/download'
// import jsPDF from 'jspdf'
import moment from 'moment'

import StyledContent from './StyledContent'
import { rangeWeek } from './helper'

const compare = (a, b) => {
  if (a.startdate.startdate > b.startdate.startdate) {
    return 1
  }

  if (a.startdate.startdate < b.startdate.startdate) {
    return -1
  }

  return 0
}

class CoursesModal extends React.Component {
  componentDidMount() {
    try {
      this.jsPDF = require('jspdf')
    } catch (error) {
      console.log(error)
    }
  }
  htmlToPDF = () => {
    const elementHandlers = {
      '#notPDF'(element, renderer) {
        return true
      },
    }

    const doc = new this.jsPDF()
    const source = document.getElementById('toPDF')
    doc.fromHTML(source, 20, 20, {
      elementHandlers,
    })
    doc.save('Stundenplan.pdf')
  }

  renderStundenplan() {
    const { courses, loading } = this.props

    if (loading) {
      return <p>Loading...</p>
    }

    if (courses) {
      const filteredCourses = courses.filter(
        curs => typeof curs.startdate !== 'undefined'
      )
      filteredCourses.sort(compare)
      console.log(filteredCourses)

      return (
        <div className="course-day">
          <div>
            {courses &&
              // courses
              //   .filter(curs => typeof curs.startdate !== 'undefined')
              filteredCourses.map(course => (
                <p key={course._id}>
                  <strong>
                    {moment(course.startdate.startdate.slice(0, 16)).format(
                      'dddd hh:mm'
                    )}
                  </strong>{' '}
                  <span>{course.course_type}</span> mit Manu Dauer:{' '}
                  {course.default_duration_minutes} min
                </p>
              ))}
          </div>
        </div>
      )
    }
  }

  render() {
    const { isOpen, toggleModal, page } = this.props
    const week = rangeWeek().start.getTime()

    return (
      <div>
        <Modal
          isOpen={isOpen}
          contentLabel="Change password"
          onRequestClose={toggleModal}
          className="courses-modal__box"
          overlayClassName="password-modal"
        >
          <StyledContent id="toPDF">
            <h1>Stundenplan {page}</h1>
            <h3>Woche von {moment(week).format('LL')}</h3>
            <button onClick={this.htmlToPDF} id="notPDF">
              <FaDownload size={24} color="#333" />
            </button>

            {this.renderStundenplan()}

            {/* <div className="course-day">
              <h4>Mo</h4>
              <div>
                {courses.map(course => {
                  return (
                    <p key={course.id}>
                      <strong>{course.start_time}</strong>{' '}
                      <span>{course.name}</span> mit {course.instructor}:{' '}
                      {course.duration} min
                    </p>
                  );
                })}
              </div>
            </div>

            <div className="course-day">
              <h4>Di</h4>
              <div>
                {courses.map(course => {
                  return (
                    <p key={course.id}>
                      <strong>{course.start_time}</strong>{' '}
                      <span>{course.name}</span> mit {course.instructor}:{' '}
                      {course.duration} min
                    </p>
                  );
                })}
              </div>
            </div>

            <div className="course-day">
              <h4>Mi</h4>
              <div>
                {courses.map(course => {
                  return (
                    <p key={course.id}>
                      <strong>{course.start_time}</strong>{' '}
                      <span>{course.name}</span> mit {course.instructor}:{' '}
                      {course.duration} min
                    </p>
                  );
                })}
              </div>
            </div>

            <div className="course-day">
              <h4>Do</h4>
              <div>
                {courses.map(course => {
                  return (
                    <p key={course.id}>
                      <strong>{course.start_time}</strong>{' '}
                      <span>{course.name}</span> mit {course.instructor}:{' '}
                      {course.duration} min
                    </p>
                  );
                })}
              </div>
            </div>

            <div className="course-day">
              <h4>Fr</h4>
              <div>
                {courses.filter((e, i) => i < 3).map(course => {
                  return (
                    <p key={course.id}>
                      <strong>{course.start_time}</strong>{' '}
                      <span>{course.name}</span> mit {course.instructor}:{' '}
                      {course.duration} min
                    </p>
                  );
                })}
              </div>
            </div>

            <div className="course-day">
              <h4>Sa</h4>
              <div>
                {courses.filter((e, i) => i < 2).map(course => {
                  return (
                    <p key={course.id}>
                      <strong>{course.start_time}</strong>{' '}
                      <span>{course.name}</span> mit {course.instructor}:{' '}
                      {course.duration} min
                    </p>
                  );
                })}
              </div>
            </div>

            <div className="course-day">
              <h4>So</h4>
              <div>
                {courses.filter((e, i) => i < 2).map(course => {
                  return (
                    <p key={course.id}>
                      <strong>{course.start_time}</strong>{' '}
                      <span>{course.name}</span> mit {course.instructor}:{' '}
                      {course.duration} min
                    </p>
                  );
                })}
              </div>
            </div> */}
          </StyledContent>
        </Modal>
      </div>
    )
  }
}

CoursesModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggleModal: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  courses: PropTypes.array,
}
CoursesModal.defaultPropr = {
  courses: [],
}

export default CoursesModal
