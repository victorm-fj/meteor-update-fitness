import React from 'react'
import PropTypes from 'prop-types'

import EditableText from '../../../components/editableText/EditableText'
import StyledEServices from './StyledEServices'

const EServices = props => (
  <section>
    <StyledEServices>
      <div style={{ position: 'relative' }}>
        <EditableText editableContent={props.editableContent} />
      </div>
      <div>{props.image && <img src={props.image} alt="E Services" />}</div>
    </StyledEServices>
  </section>
)

EServices.propTypes = {
  editableContent: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
}
EServices.defaultProps = {
  editableContent: '',
  image: '',
}

export default EServices
