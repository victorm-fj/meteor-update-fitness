import React from 'react'
import PropTypes from 'prop-types'

import EditableText from '../../../components/editableText/EditableText'
import EditableImage from '../../../components/editableImage/EditableImage'
import StyledFitness from './StyledFitness'

const Fitness = props => (
  <section>
    <StyledFitness>
      <div>
        <EditableImage alt="Standorte Fitness" image={props.image} />
      </div>
      <div style={{ flex: 0.1 }} />
      <div style={{ position: 'relative' }}>
        <EditableText editableContent={props.editableContent} />
      </div>
    </StyledFitness>
  </section>
)

Fitness.propTypes = {
  editableContent: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
}
Fitness.defaultProps = {
  editableContent: '',
  image: '',
}

export default Fitness
