import React from 'react'
import PropTypes from 'prop-types'

import EditableText from '../../../components/editableText/EditableText'
import StyledSchedule from './StyledSchedule'

const Schedule = props => (
  <section>
    <StyledSchedule>
      <div style={{ position: 'relative' }}>
        <EditableText editableContent={props.editableContent} />
      </div>
      <div>
        <p>Montag, Miwoch,Freitag</p>
        <p>09.00 - 21.30 Uhr</p>
      </div>
      <div>
        <p>Samstag, Sonntag und Feiertag</p>
        <p>06.30 - 21.30 Uhr</p>
      </div>
      <div>
        <p>Samstag, Sonntag und Feiertage</p>
        <p>09.00 - 14.00 Uhr</p>
      </div>
    </StyledSchedule>
  </section>
)

Schedule.propTypes = {
  editableContent: PropTypes.string.isRequired,
}
Schedule.defaultProps = {
  editableContent: '',
}

export default Schedule
