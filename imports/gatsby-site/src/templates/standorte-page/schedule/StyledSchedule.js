import styled from 'styled-components';

const StyledSchedule = styled.div`
  & > div {
    display: flex;
    width: 40%;
    justify-content: space-between;
  }

  @media (max-width: 1200px) {
    & > div {
      width: 50%;
    }
  }

  @media (max-width: 1000px) {
    & > div {
      width: 60%;
    }
  }

  @media (max-width: 800px) {
    & > div {
      width: 80%;
    }
  }

  @media (max-width: 700px) {
    & > div {
      width: 90%;
    }
  }

  @media (max-width: 600px) {
    & > div {
      flex-direction: column;
      margin-bottom: 1.4rem;
    }
  }
`;

export default StyledSchedule;
