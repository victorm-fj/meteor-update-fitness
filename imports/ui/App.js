import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';

import { Status } from '../api/status';
import ScrollToTop from './ScrollToTop';
import theme from './theme';
import Header from './layouts/header/Header';
import Footer from './layouts/footer/Footer';
import Home from './pages/home/Home';
import Standorte from './pages/standorte/Standorte';
import StandortePage from './pages/standorte-page/StandortePage';
import Angebote from './pages/angebote/Angebote';
import Probetraining from './pages/probetraining/Probetraining';
import Mitglied from './pages/mitglied/Mitglied';
import NotFound from './pages/NotFound';
import requireAuth from './components/requireAuth';
import Login from './components/login/Login';

class App extends React.Component {
	componentDidUpdate(prevProps) {
		if (
			prevProps.deployment &&
			prevProps.deployment.status !== this.props.deployment.status
		) {
			switch (this.props.deployment.status) {
				case 'failed':
					toast.error('Deployment failed :(');
					break;
				case 'succeeded':
					toast.success('Deployment succeeded :)');
					break;
				case 'started':
					toast.info('A new build has started!');
					break;
				default:
					break;
			}
		}
	}
	render() {
		return (
			<BrowserRouter>
				<ScrollToTop>
					<ThemeProvider theme={theme}>
						<div
							style={{
								// height: '100vh',
								display: 'flex',
								flexDirection: 'column',
								justifyContent: 'space-between',
							}}
						>
							<ToastContainer
								autoClose={10000}
								closeButton={false}
								className="toast-container"
							/>
							<Header />

							<Switch>
								<Route path="/login" component={Login} />
								<Route exact path="/" component={requireAuth(Home)} />
								<Route
									exact
									path="/standorte"
									component={requireAuth(Standorte)}
								/>
								<Route
									path="/standorte/:page"
									component={requireAuth(StandortePage)}
								/>
								<Route path="/angebote" component={requireAuth(Angebote)} />
								<Route
									path="/probetraining"
									component={requireAuth(Probetraining)}
								/>
								<Route path="/mitglied" component={requireAuth(Mitglied)} />
								<Route component={NotFound} />
							</Switch>

							<Footer />
						</div>
					</ThemeProvider>
				</ScrollToTop>
			</BrowserRouter>
		);
	}
}

export default withTracker(() => {
	Meteor.subscribe('status');
	return {
		deployment: Status.findOne({ type: 'deploy' }),
	};
})(App);
