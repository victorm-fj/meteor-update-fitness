import styled from 'styled-components';

const StyledButton = styled.button`
  padding: 0.7rem;
  margin-right: 1rem;
  border: none;
  cursor: pointer;
  outline: none;
  background: none;
  text-decoration: underline;
`;

export default StyledButton;
