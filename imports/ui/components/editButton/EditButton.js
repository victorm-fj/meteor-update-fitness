import React from 'react';
import PropTypes from 'prop-types';
import MdCreate from 'react-icons/lib/md/create';

import StyledEditButton from './StyledEditButton';

const EditButton = ({ containerStyle, onClick, size }) => (
  <div style={containerStyle}>
    <StyledEditButton onClick={onClick}>
      <MdCreate size={size} color="#8c8c8c" />
    </StyledEditButton>
  </div>
);

EditButton.defaultProps = {
  size: 24,
};

EditButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  containerStyle: PropTypes.object,
  size: PropTypes.number,
};

export default EditButton;
