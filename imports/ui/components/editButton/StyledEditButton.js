import styled from 'styled-components';

const StyledEditButton = styled.button`
  background: none;
  border: none;
  outline: none;
  cursor: pointer;
`;

export default StyledEditButton;
