import React from 'react';
import PropTypes from 'prop-types';

import StyledEditableImage from './StyledEditableImage';
import UploadImagesModal from '../uploadImages/UploadImagesModal';
import EditButton from '../editButton/EditButton';

const EditableImage = props => (
  <StyledEditableImage>
    <UploadImagesModal
      isOpen={props.editImage}
      toggleModal={props.toggleEditImage}
      page={props.page}
      component={props.component}
    />

    <EditButton
      containerStyle={props.containerStyle}
      onClick={props.toggleEditImage}
    />

    {props.image && <img src={props.image.imageUrl} alt={props.alt} />}
  </StyledEditableImage>
);

EditableImage.defaultProps = {
  alt: '',
  containerStyle: { position: 'absolute', right: '-15%', top: '-10%' },
};

EditableImage.propTypes = {
  page: PropTypes.string.isRequired,
  component: PropTypes.string.isRequired,
  alt: PropTypes.string,
  editImage: PropTypes.bool.isRequired,
  toggleEditImage: PropTypes.func.isRequired,
  containerStyle: PropTypes.object,
  image: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    imageId: PropTypes.string,
    imageUrl: PropTypes.string,
    updatedAt: PropTypes.number,
  }),
};

export default EditableImage;
