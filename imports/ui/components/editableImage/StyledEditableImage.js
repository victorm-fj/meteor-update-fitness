import styled from 'styled-components';

const StyledEditableImage = styled.div`
  position: relative;
`;

export default StyledEditableImage;
