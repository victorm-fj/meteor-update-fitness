import React from 'react';
import PropTypes from 'prop-types';

import StyledEditableText from './StyledEditableText';
import SimpleEditor from '../simpleEditor/SimpleEditor';
import EditButton from '../editButton/EditButton';

class EditableText extends React.Component {
  renderEditor() {
    if (this.props.editable) {
      return (
        <SimpleEditor
          page={this.props.page}
          component={this.props.component}
          toggleEditor={this.props.toggleEditor}
          editableContent={this.props.editableContent}
          call={this.props.call}
          complex={this.props.complex}
        />
      );
    }

    if (this.props.editableContent) {
      return (
        <div
          style={{ width: '100%' }}
          dangerouslySetInnerHTML={{
						__html: this.props.editableContent.htmlContent
					}}
        />
      );
    }

    return null;
  }

  render() {
    return (
      <StyledEditableText>
        {!this.props.editable && (
        <EditButton
          onClick={this.props.toggleEditor}
          containerStyle={this.props.containerStyle}
        />
				)}

        {/* Editable Content */}
        {this.renderEditor()}
      </StyledEditableText>
    );
  }
}

EditableText.defaultProps = {
  containerStyle: { position: 'absolute', right: '-2.2rem', top: '-2rem' },
  complex: false
};

EditableText.propTypes = {
  editable: PropTypes.bool.isRequired,
  toggleEditor: PropTypes.func.isRequired,
  page: PropTypes.string.isRequired,
  component: PropTypes.string.isRequired,
  editableContent: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    htmlContent: PropTypes.string,
    updatedAt: PropTypes.number,
    link: PropTypes.string
  }),
  call: PropTypes.func.isRequired,
  containerStyle: PropTypes.object,
  complex: PropTypes.bool
};

export default EditableText;
