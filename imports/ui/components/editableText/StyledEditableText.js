import styled from 'styled-components';

const StyledEditableText = styled.div`
	width: 100%;
	display: inline-flex;
	position: relative;
`;

export default StyledEditableText;
