import React from 'react';
import PropTypes from 'prop-types';
import FaClose from 'react-icons/lib/fa/close';
import FaCheck from 'react-icons/lib/fa/check';

import EditButton from '../editButton/EditButton';
import StyledButton from '../StyledButton';
import StyledEditableVideo from './StyledEditableVideo';

class EditableVideo extends React.Component {
  state = {
    editVideo: false,
    srcVideo: '',
    videoError: '',
  };

  onChangeLink = e => {
    this.setState({ srcVideo: e.target.value });
  };

  getVideoId() {
    const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    const match = this.state.srcVideo.match(regExp);

    if (match && match[2].length === 11) {
      return match[2];
    }

    return 'error';
  }

  toggleEditVideo = () => {
    this.setState({
      editVideo: !this.state.editVideo,
      srcVideo: '',
      videoError: '',
    });
  };

  saveLink = () => {
    const videoId = this.getVideoId();

    if (videoId !== 'error') {
      const { page, component, call } = this.props;
      const link = `https://www.youtube.com/embed/${videoId}`;
      call('save.link', page, component, link, err => {
        console.log(err);
      });
      this.toggleEditVideo();
    } else {
      this.setState({ videoError: 'Insert a valid youtube URL' });
    }
  };

  renderVideo() {
    if (this.props.editableContent) {
      return (
        <iframe
          title="Probetraining im update Fitness"
          width="100%"
          height="250"
          src={this.props.editableContent.link}
          frameBorder="0"
          allowFullScreen
        />
      );
    }

    return null;
  }

  render() {
    return (
      <StyledEditableVideo>
        {!this.state.editVideo && (
          <EditButton
            containerStyle={{ textAlign: 'right', marginBottom: '1rem' }}
            onClick={this.toggleEditVideo}
          />
        )}

        {this.renderVideo()}

        {this.state.editVideo && (
          <div style={{ marginTop: '1rem' }}>
            {this.state.videoError && <p>{this.state.videoError}</p>}

            <input
              type="text"
              placeholder="Enter video URL"
              onChange={this.onChangeLink}
            />

            <div className="actions">
              <StyledButton onClick={this.toggleEditVideo}>
                <FaClose size={24} color="#8c8c8c" />
              </StyledButton>

              <StyledButton onClick={this.saveLink}>
                <FaCheck size={24} color="#8c8c8c" />
              </StyledButton>
            </div>
          </div>
        )}
      </StyledEditableVideo>
    );
  }
}

EditableVideo.propTypes = {
  page: PropTypes.string.isRequired,
  component: PropTypes.string.isRequired,
  editableContent: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    htmlContent: PropTypes.string,
    updatedAt: PropTypes.number,
    link: PropTypes.string,
  }),
  call: PropTypes.func.isRequired,
};

export default EditableVideo;
