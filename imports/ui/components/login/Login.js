import React from 'react';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';

import StyledLogin from './StyledLogin';

class Login extends React.Component {
  state = { username: '', password: '', error: '' };

  onSubmit = e => {
    e.preventDefault();
    const username = this.state.username.trim();
    const password = this.state.password.trim();

    Meteor.loginWithPassword({ username }, password, err => {
      if (!err) this.props.history.replace('/');
      else this.setState({ error: err.reason });
    });
  };

  onUsernameChange = e => {
    const username = e.target.value;
    this.setState({ username });
  };

  onPasswordChange = e => {
    const password = e.target.value;
    this.setState({ password });
  };

  render() {
    return (
      <StyledLogin>
        <h1>update Fitness Admin app</h1>

        {this.state.error && <p>{this.state.error}</p>}

        <form noValidate onSubmit={this.onSubmit}>
          <input
            type="text"
            placeholder="Username"
            value={this.state.username}
            onChange={this.onUsernameChange}
          />
          <input
            type="password"
            placeholder="Password"
            value={this.state.password}
            onChange={this.onPasswordChange}
          />
          <button>Login</button>
        </form>
      </StyledLogin>
    );
  }
}

Login.propTypes = {
  history: PropTypes.object.isRequired,
};

export default Login;
