import styled from 'styled-components';

const StyledLogin = styled.div`
	height: 100vh;
	flex: 1;
	background: ${props => props.theme.gray};
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;

	& > form {
		display: flex;
		flex-direction: column;
	}

	input,
	button {
		margin: 1rem;
		padding: 1rem;
	}
`;

export default StyledLogin;
