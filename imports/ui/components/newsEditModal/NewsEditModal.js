import React from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import FlipMove from 'react-flip-move';
import FaArrowDown from 'react-icons/lib/fa/arrow-down';
import FaArrowUp from 'react-icons/lib/fa/arrow-up';
import FaTrash from 'react-icons/lib/fa/trash';

import StyledContent from './StyledContent';
import StyledButton from '../StyledButton';
import EditableElement from '../editableElement/EditableElement';

class NewsEditModal extends React.Component {
  render() {
    const { isOpen, toggleModal, news, page } = this.props;
    return (
      <div>
        <Modal
          isOpen={isOpen}
          contentLabel="Change password"
          onRequestClose={toggleModal}
          className="news-modal__box"
          overlayClassName="password-modal"
        >
          <StyledContent>
            <StyledButton style={{ alignSelf: 'flex-end' }} onClick={() => Meteor.call('save.news', page, 'Title', 'Body')}>
              Add
            </StyledButton>

            <FlipMove maintainContainerHeight>
              {news.length > 0 &&
                news.map(item => (
                    <div key={item._id}>
                      <div className="content">
                        {/* <p>
                          <strong>{item.title}</strong>
                        </p>
                        <p>{item.body}</p> */}
                        <EditableElement
                          value={item.title}
                          element="p"
                          inputStyle={{ fontWeight: 700, width: '100%' }}
                          elementStyle={{ fontWeight: 700 }}
                          onChange={value => Meteor.call('update.title', item._id, value)}
                        />

                        <EditableElement
                          value={item.body}
                          element="p"
                          inputStyle={{ width: '100%' }}
                          onChange={value => Meteor.call('update.body', item._id, value)}
                        />
                      </div>

                      <div className="actions">
                        <button
                          onClick={() => Meteor.call('update.order', item._id, 1)}
                        >
                          <FaArrowUp size={18} color="#333" />
                        </button>

                        <button
                          onClick={() =>
                            Meteor.call('update.order', item._id, -1)
                          }
                        >
                          <FaArrowDown size={18} color="#333" />
                        </button>

                        <p>Order: {item.order}</p>

                        <button
                          onClick={() => {
                            Meteor.call('remove.news', item._id)
                          }}
                        >
                          <FaTrash size={18} color="#333" />
                        </button>
                      </div>
                    </div>
                  ))}

              {news.length === 0 && <h1>No news for this slider</h1>}
            </FlipMove>
          </StyledContent>
        </Modal>
      </div>
    );
  }
}


NewsEditModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggleModal: PropTypes.func.isRequired,
  news: PropTypes.arrayOf(PropTypes.object),
};

export default NewsEditModal;
