import styled from 'styled-components';

const StyledContent = styled.div`
  display: flex;
  flex-direction: column;

  & > div > div {
    padding: 1rem 0;
  }

  .editable {
    border-bottom: 1px solid black;
  }

  .actions {
    display: flex;
    align-items: center;

    p {
      margin-bottom: 0;
    }
  }

  button {
    padding: 0.7rem;
    margin-right: 1rem;
    border: none;
    cursor: pointer;
    outline: none;
    background: none;
  }
`;

export default StyledContent;
