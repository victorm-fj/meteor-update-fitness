import React from 'react';
import { Meteor } from 'meteor/meteor';

export default WrappedComponent => {
  class RequireAuth extends React.Component {
    componentWillMount() {
      if (!Meteor.userId()) this.props.history.replace('/login');
    }

    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  return RequireAuth;
};
