import React from 'react';
import PropTypes from 'prop-types';
import FaClose from 'react-icons/lib/fa/close';
import FaCheck from 'react-icons/lib/fa/check';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import { Meteor } from 'meteor/meteor';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import StyledSimpleEditor from './StyledSimpleEditor';

const uploadCallback = file =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = event => {
      const imageDataURL = event.target.result;

      Meteor.call('images.uploadAndReturnURL', imageDataURL, (err, result) => {
        if (err) {
          console.log(err);
          reject(err);
        }
        console.log(result);
        resolve(result);
      });
    };
  });

const toolbar = { options: ['inline', 'blockType', 'fontSize', 'list', 'textAlign'] };
const complexToolbar = {
  options: [
    'inline',
    'blockType',
    'fontSize',
    'fontFamily',
    'list',
    'textAlign',
    'colorPicker',
    'link',
    'embedded',
    'emoji',
    'image',
    'remove',
    'history'
  ],
  image: {
    previewImage: true,
    alignmentEnabled: true,
    uploadCallback,
    inputAccept: 'image/gif,image/jpeg,image/jpg,image/png,image/svg',
    alt: { present: true, mandatory: true },
    defaultSize: {
      height: 'auto',
      width: 'auto'
    }
  }
};

class SimpleEditor extends React.Component {
	state = { editorState: EditorState.createEmpty() };

	componentWillMount() {
	  const { editableContent } = this.props;
	  if (editableContent) {
	    const contentBlock = htmlToDraft(editableContent.htmlContent);
	    const contentState = ContentState.createFromBlockArray(
	      contentBlock.contentBlocks,
	      contentBlock.entityMap
	    );
	    const editorState = EditorState.createWithContent(contentState);
	    this.setState({ editorState });
	  }
	}

	onEditorStateChange = editorState => {
	  this.setState({ editorState });
	};

	saveContent = () => {
	  const { page, component, toggleEditor } = this.props;
	  const rawContent = convertToRaw(this.state.editorState.getCurrentContent());
	  const htmlContent = draftToHtml(rawContent);

	  this.props.call('save.content', page, component, htmlContent, err => {
	    console.log(err);
	  });

	  toggleEditor();
	};

	render() {
	  return (
  <StyledSimpleEditor>
    <Editor
					// wrapperClassName="home-wrapper"
					// editorClassName="home-editor"
					// toolbarClassName="toolbar-class"
      editorState={this.state.editorState}
      editorStyle={{
						border: '1px solid #999',
						padding: '10px',
						borderRadius: '2px'
					}}
      onEditorStateChange={this.onEditorStateChange}
      toolbar={this.props.complex ? complexToolbar : toolbar}
    />
    <div className="actions">
      <button onClick={this.props.toggleEditor}>
        <FaClose size={24} color="#8c8c8c" />
      </button>

      <button onClick={this.saveContent}>
        <FaCheck size={24} color="#8c8c8c" />
      </button>
    </div>
  </StyledSimpleEditor>
	  );
	}
}

SimpleEditor.propTypes = {
  editableContent: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    htmlContent: PropTypes.string,
    updatedAt: PropTypes.number
  }),
  toggleEditor: PropTypes.func.isRequired,
  page: PropTypes.string.isRequired,
  component: PropTypes.string.isRequired,
  call: PropTypes.func.isRequired,
  complex: PropTypes.bool.isRequired
};

export default SimpleEditor;
