import styled from 'styled-components';

const StyledSimpleEditor = styled.div`
	.actions {
		display: flex;
		justify-content: flex-end;

		button {
			background: none;
			border: none;
			outline: none;
			cursor: pointer;
		}
	}
`;

export default StyledSimpleEditor;
