import styled from 'styled-components';

const StyledItem = styled.div`
  position: relative;
  color: ${props => props.theme.darkBackground};
  z-index: 10;

  .news {
    position: absolute;
    height: 15%;
    width: 100%;
    bottom: 0;
    left: 0;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .news-title,
  .news-body {
    display: flex;
    align-items: center;
    padding-left: 1.8vw;

    p {
      font-size: 1.8vw;
      line-height: 2;
      white-space: normal;
      margin-bottom: 0;
    }
  }

  .news-title {
    background: rgba(187, 218, 11, 0.7);
    width: 30%;
    height: 100%;
  }

  .news-body {
    background: rgba(255, 255, 255, 0.7);
    width: 70%;
    height: 100%;
  }

  .news-body-content {
    width: 80%;
    display: flex;
    align-items: center;

    p {
      font-size: 1.3vw;
    }
  }

  @media only screen and (max-width: 640px) {
    .news {
      display: none;
      visibility: hidden;
    }
  }
`;

export default StyledItem;
