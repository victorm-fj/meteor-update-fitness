import React from 'react';
import Modal from 'react-modal';
import PropTypes from 'prop-types';
import ImageUploader from './imageUploader/ImageUploader';

const UploadImagesModal = ({
  isOpen,
  toggleModal,
  page,
  component,
  images,
  returnURL
}) => (
  <div>
    <Modal
      isOpen={isOpen}
      contentLabel="Upload images"
      onRequestClose={toggleModal}
      className="images-uploader__modal-box"
      overlayClassName="password-modal"
      ariaHideApp={false}
    >
      <ImageUploader
        page={page}
        component={component}
        toggleModal={toggleModal}
        images={images}
        returnURL={returnURL}
      />
    </Modal>
  </div>
);

UploadImagesModal.defaultProps = {
  returnURL: false
};
UploadImagesModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggleModal: PropTypes.func.isRequired,
  page: PropTypes.string.isRequired,
  component: PropTypes.string.isRequired,
  images: PropTypes.arrayOf(PropTypes.object),
  returnURL: PropTypes.bool
};
export default UploadImagesModal;
