import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import {
	SortableContainer,
	SortableElement,
	arrayMove,
} from 'react-sortable-hoc';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import StyledImageUploader from './StyledImageUploader';
import StyledFileContainer from './StyledFileContainer';
import StyledErrorsContainer from './StyledErrorsContainer';
import StyledButton from './StyledButton';
import StyledUploadPictures from './StyledUploadPictures';
import StyledUploadPictureContainer from './StyledUploadPictureContainer';
import StyledDeleteImage from './StyledDeleteImage';

const PicturePreviewItem = SortableElement(({ image, remove }) => {
	return (
		<StyledUploadPictureContainer>
			<StyledDeleteImage onClick={() => remove(image)}>X</StyledDeleteImage>
			<img src={image.imgSrc} alt="preview" />
		</StyledUploadPictureContainer>
	);
});
const PicturePreviewList = SortableContainer(({ images, remove }) => {
	return (
		<StyledUploadPictures>
			{images.map((image, index) => (
				<PicturePreviewItem
					key={`item-${index}`}
					index={index}
					image={image}
					remove={remove}
				/>
			))}
		</StyledUploadPictures>
	);
});

class ImageUploader extends React.Component {
	state = {
		notAcceptedFileType: [],
		notAcceptedFileSize: [],
		loading: false,
		uploadError: '',
		images: this.props.images || [],
		imageURL: '',
	};
	onUploadImages = async () => {
		const { images } = this.state;
		const { page, component, returnURL } = this.props;
		this.toggleLoading();
		if (returnURL) {
			const image = images[0];
			try {
				const imageURL = await Meteor.callPromise(
					'images.uploadAndReturnURL',
					image
				);
				this.setState({ imageURL });
				this.toggleLoading();
			} catch (error) {
				console.log('images.uploadAndReturnURL error', error);
				this.setState({ uploadError: error.reason });
			}
			return;
		}
		if (images.length === 1 && component !== 'slider') {
			const image = images[0];
			try {
				await Meteor.callPromise('images.upload', image, page, component);
				this.toggleLoading();
				this.props.toggleModal();
			} catch (error) {
				console.log('images.upload error', error);
				this.setState({ uploadError: error.reason });
			}
		} else {
			try {
				await Meteor.callPromise(
					'images.uploadMultiple',
					images,
					page,
					component
				);
				this.toggleLoading();
				this.props.toggleModal();
			} catch (error) {
				console.log('images.uploadMultiple error', error);
				this.setState({ uploadError: error.reason });
			}
		}
	};
	toggleLoading() {
		this.setState({ loading: !this.state.loading });
	}
	// Handle file validation
	onDropFile = e => {
		const { files } = e.target;
		const f = files[0];
		// Check for file extension
		if (!this.hasExtension(f.name)) {
			this.setState(prevState => ({
				notAcceptedFileSize: [...prevState.notAcceptedFileSize, f.name],
			}));
		} else if (f.size > this.props.maxFileSize) {
			this.setState(prevState => ({
				notAcceptedFileSize: [...prevState.notAcceptedFileSize, f.name],
			}));
		} else {
			const reader = new FileReader();
			reader.onload = event => {
				const imageDataURL = event.target.result;
				this.setState(prevState => {
					if (
						prevState.images.filter(image => image.imgSrc === imageDataURL)
							.length === 0
					) {
						return { images: [...prevState.images, { imgSrc: imageDataURL }] };
					}
				});
			};
			reader.readAsDataURL(f);
		}
	};
	// Check file extension (onDropFile)
	hasExtension(fileName) {
		return new RegExp(
			`(${this.props.imgExtension.join('|').replace(/\./g, '\\.')})$`
		).test(fileName);
	}
	// On button click, trigger input file to open
	triggerFileUpload = () => {
		this.input.click();
	};
	// Remove image from state
	removeImage = picture => {
		this.setState(prevState => ({
			images: [
				...prevState.images.filter(image => image.imgSrc !== picture.imgSrc),
			],
		}));
	};
	renderLabel() {
		if (this.props.withLabel) {
			return <p>{this.props.label}</p>;
		}
		return null;
	}
	// Render errors if any
	renderErrors() {
		let notAccepted = '';
		if (this.state.notAcceptedFileType.length > 0) {
			notAccepted = this.state.notAcceptedFileType.map((error, index) => (
				<div key={index}>
					* {error} {this.props.fileTypeError}
				</div>
			));
		}
		if (this.state.notAcceptedFileSize.length > 0) {
			notAccepted = this.state.notAcceptedFileSize.map((error, index) => (
				<div key={index}>
					* {error} {this.props.fileSizeError}
				</div>
			));
		}
		return notAccepted;
	}
	onSortEnd = ({ oldIndex, newIndex }) => {
		this.setState(prevState => ({
			images: arrayMove(prevState.images, oldIndex, newIndex),
		}));
	};
	// Render preview of images
	renderPreview() {
		return (
			<PicturePreviewList
				images={this.state.images}
				remove={this.removeImage}
				onSortEnd={this.onSortEnd}
				axis="xy"
				pressDelay={200}
			/>
		);
	}
	renderUploader() {
		return (
			<div>
				{this.state.uploadError && (
					<h3 style={{ textAlign: 'center', color: '#DA0B54' }}>
						{this.state.uploadError}
					</h3>
				)}
				<BlockUi tag="div" blocking={this.state.loading}>
					<StyledFileContainer>
						{this.renderLabel()}
						<StyledErrorsContainer>{this.renderErrors()}</StyledErrorsContainer>
						<StyledButton onClick={this.triggerFileUpload}>
							{this.props.buttonText}
						</StyledButton>
						<input
							type="file"
							ref={input => (this.input = input)}
							multiple="multiple"
							onChange={this.onDropFile}
							accept={this.props.accept}
						/>
						{this.renderPreview()}
					</StyledFileContainer>
					<div
						style={{
							display: 'flex',
							justifyContent: 'flex-end',
						}}
					>
						<StyledButton
							onClick={this.onUploadImages}
							disabled={
								!!this.state.uploadError || this.state.images.length === 0
							}
							className={
								(this.state.uploadError || this.state.images.length === 0) &&
								'no-transition'
							}
						>
							Upload
						</StyledButton>
					</div>
				</BlockUi>
			</div>
		);
	}
	renderResult() {
		const { imageURL } = this.state;
		return (
			<div style={{ textAlign: 'center' }}>
				<img src={imageURL} alt="Uploaded image" style={{ marginBottom: 40 }} />
				<br />
				<p>{imageURL}</p>
			</div>
		);
	}
	render() {
		console.log('ImageUploader images state', this.state.images);
		const { imageURL } = this.state;
		const { returnURL } = this.props;
		return (
			<StyledImageUploader>
				{returnURL
					? imageURL !== '' ? this.renderResult() : this.renderUploader()
					: this.renderUploader()}
			</StyledImageUploader>
		);
	}
}

ImageUploader.defaultProps = {
	accept: 'accept=image/*',
	buttonText: 'Choose images',
	withLabel: true,
	label: 'Max file size: 5mb, accepted: jpg|gif|png|gif',
	imgExtension: ['.jpg', '.gif', '.png', '.gif'],
	maxFileSize: 5242880,
	fileSizeError: ' file size is too big',
	fileTypeError: ' is not supported file extension',
};
ImageUploader.propTypes = {
	accept: PropTypes.string,
	buttonText: PropTypes.string,
	withLabel: PropTypes.bool,
	label: PropTypes.string,
	imgExtension: PropTypes.array,
	maxFileSize: PropTypes.number,
	fileSizeError: PropTypes.string,
	fileTypeError: PropTypes.string,
	page: PropTypes.string.isRequired,
	component: PropTypes.string.isRequired,
	toggleModal: PropTypes.func.isRequired,
	images: PropTypes.arrayOf(PropTypes.object),
	returnURL: PropTypes.bool.isRequired,
};
export default ImageUploader;
