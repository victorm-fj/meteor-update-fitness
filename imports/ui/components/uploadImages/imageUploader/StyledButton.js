import styled from 'styled-components';

const StyledButton = styled.button`
  padding: 6px 23px;
  background: #3f4257;
  border-radius: 30px;
  color: white;
  font-weight: 300;
  font-size: 14px;
  margin: 10px 0;
  transition: all 0.2s ease-in;
  cursor: pointer;
  outline: none;
  border: none;

  &:hover:not(.no-transition) {
    background: #545972;
  }

  &:disabled {
    opacity: 0.5;
    cursor: auto;
  }

  .no-transition {
    transition: none;
  }
`;

export default StyledButton;
