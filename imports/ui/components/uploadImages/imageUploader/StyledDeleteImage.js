import styled from 'styled-components';

const StyledDeleteImage = styled.div`
  position: absolute;
  top: -9px;
  right: -9px;
  font-size: 8px;
  color: ${props => props.theme.white};
  background: ${props => props.theme.brand};
  border-radius: 50%;
  width: 15px;
  height: 15px;
  text-align: center;
  line-height: 15px;
  cursor: pointer;
`;

export default StyledDeleteImage;
