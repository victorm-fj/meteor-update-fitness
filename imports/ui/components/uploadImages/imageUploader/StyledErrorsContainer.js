import styled from 'styled-components';

const StyledErrorsContainer = styled.div`
  max-width: 300px;
  font-size: 12px;
  color: red;
  text-align: left;
`;

export default StyledErrorsContainer;
