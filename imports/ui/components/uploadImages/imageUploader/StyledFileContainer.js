import styled from 'styled-components';

const StyledFileContainer = styled.div`
  background: ${props => props.theme.white};
  box-shadow: 0 0 5px ${props => props.theme.boxShadow};
  height: 60vh;
  overflow: scroll;
  position: relative;
  border-radius: 10px;
  padding: 20px 0;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  margin: 10px auto;
  transition: all 0.3s ease-in;

  input {
    opacity: 0;
    position: absolute;
    z-index: -1;
  }

  p {
    font-size: 12px;
    margin: 8px 0 4px;
  }
`;

export default StyledFileContainer;
