import styled from 'styled-components';

const StyledImageUploader = styled.div`
  width: 100%;
`;

export default StyledImageUploader;
