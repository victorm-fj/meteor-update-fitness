import styled from 'styled-components';

const StyledUploadPictureContainer = styled.div`
  width: 25%;
  margin: 1%;
  padding: 10px;
  background: #edf2f6;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 90px;
  box-shadow: 0 0 8px 2px rgba(0, 0, 0, 0.1);
  border: 1px solid #d0dbe4;
  position: relative;

  img {
    width: 70%;
    height: 100%;
  }
`;

export default StyledUploadPictureContainer;
