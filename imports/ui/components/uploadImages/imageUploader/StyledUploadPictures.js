import styled from 'styled-components';

const StyledUploadPictures = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  width: 100%;
`;

export default StyledUploadPictures;
