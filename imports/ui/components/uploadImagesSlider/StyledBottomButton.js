import styled from 'styled-components';

const StyledBottomButton = styled.button`
  background: none;
  border: none;
  outline: none;
  cursor: pointer;
  position: absolute;
  bottom: 4rem;
  right: 1rem;
  z-index: 10;

  svg {
    filter: drop-shadow(2px 2px 2px #1a1a1a);
  }

  svg:hover {
    fill: ${props => props.theme.brand};
  }
`;

export default StyledBottomButton;
