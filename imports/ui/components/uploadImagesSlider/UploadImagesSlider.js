import React from 'react';
import PropTypes from 'prop-types';
import MdCreate from 'react-icons/lib/md/create';
import Slider from '../slider/Slider';
import UploadImagesModal from '../uploadImages/UploadImagesModal';
import StyledUploadImagesSlider from './StyledUploadImagesSlider';
import StyledTopButton from './StyledTopButton';
import StyledBottomButton from './StyledBottomButton';
import NewsEditModal from '../newsEditModal/NewsEditModal';

const UploadImagesSlider = ({
  page,
  component,
  editable,
  toggleEdit,
  images,
  editableNews,
  toggleEditNews,
  news
}) => (
  <StyledUploadImagesSlider>
    <UploadImagesModal
      isOpen={editable}
      toggleModal={toggleEdit}
      page={page}
      component={component}
      images={images}
    />
    <NewsEditModal
      isOpen={editableNews}
      toggleModal={toggleEditNews}
      news={news}
      page={page}
    />
    <StyledTopButton onClick={toggleEdit}>
      <MdCreate size={28} color="#bcbcbc" />
    </StyledTopButton>
    <StyledBottomButton onClick={toggleEditNews}>
      <MdCreate size={28} color="#bcbcbc" />
    </StyledBottomButton>
    {images && <Slider images={images} />}
  </StyledUploadImagesSlider>
);

UploadImagesSlider.propTypes = {
  page: PropTypes.string.isRequired,
  component: PropTypes.string.isRequired,
  editable: PropTypes.bool.isRequired,
  toggleEdit: PropTypes.func.isRequired,
  images: PropTypes.arrayOf(PropTypes.object),
  news: PropTypes.arrayOf(PropTypes.object),
  editableNews: PropTypes.bool.isRequired,
  toggleEditNews: PropTypes.func.isRequired
};
export default UploadImagesSlider;
