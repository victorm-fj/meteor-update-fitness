import styled from 'styled-components';

const StyledEditor = styled.div`
    body {
		background: #eee;

		color: #333333;
		font-size: 18px;
		line-height: 28px;

		font-family: 'Open Sans', serif;
	}

	p {
		margin: 0 0 28px;
	}

	pre {
		overflow-x: auto;
	}

	.container {
		background-color: white;
	}

	.ory-cell-leaf {
		padding: 8px;
	}

	.container {
		background: white;
		padding: 0 24px;
	}

	.container {
		margin-right: auto;
		margin-left: auto;
		align-items: stretch;
	}

	@media (min-width: 768px) {
		.container {
			width: 750px;
		}
	}

	@media (min-width: 992px) {
		.container {
			width: 970px;
		}
	}

	@media (min-width: 1200px) {
		.container {
			width: 1170px;
		}
	}

	.editable,
	#editable-static {
		padding: 5px;
	}
`;

export default StyledEditor;
