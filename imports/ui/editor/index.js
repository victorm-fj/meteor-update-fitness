import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
// The editor core
import Editor, { Editable, createEmptyState } from 'ory-editor-core';
import 'ory-editor-core/lib/index.css'; // we also want to load the stylesheets
// Require our ui components (optional). You can implement and use your own ui too!
import { Trash, DisplayModeToggle, Toolbar } from 'ory-editor-ui';
import 'ory-editor-ui/lib/index.css';
// The rich text area plugin
import slate from 'ory-editor-plugins-slate';
import 'ory-editor-plugins-slate/lib/index.css';
// The spacer plugin
import spacer from 'ory-editor-plugins-spacer';
import 'ory-editor-plugins-spacer/lib/index.css';
// The image plugin
import image from 'ory-editor-plugins-image';
import 'ory-editor-plugins-image/lib/index.css';
// The video plugin
import video from 'ory-editor-plugins-video';
import 'ory-editor-plugins-video/lib/index.css';
// The parallax plugin
import parallax from 'ory-editor-plugins-parallax-background';
import 'ory-editor-plugins-parallax-background/lib/index.css';
// The native handler plugin
import native from 'ory-editor-plugins-default-native';
// The divider plugin
import divider from 'ory-editor-plugins-divider';
import StyledEditor from './StyledEditor';
import { Pages } from '../../api/pages';

require('react-tap-event-plugin')(); // react-tap-event-plugin is required by material-ui which is used by ory-editor-ui so we need to call it here
// Define which plugins we want to use. We only have slate and parallax available, so load those.
const plugins = {
  content: [slate(), spacer, image, video, divider],
  layout: [
    /* parallax({ defaultPlugin: slate() }) */
  ],
  // If you pass the native key the editor will be able to handle native drag and drop events (such as links, text, etc).
  // The native plugin will then be responsible to properly display the data which was dropped onto the editor.
  native
};
// Creates an empty editable
const content = createEmptyState();
// Instantiate the editor
const editor = new Editor({
  defaultPlugin: slate(),
  plugins,
  // pass the content state - you can add multiple editables here
  editables: [content]
});

class OryEditor extends React.Component {
  constructor(props) {
    super(props);
    if (typeof this.props.content !== 'undefined') {
      this.editorState = this.props.content.content;
    } else {
      this.editorState = content;
    }
    editor.trigger.editable.update(this.editorState);
  }
  componentWillReceiveProps(nextProps) {
    if (typeof nextProps.content !== 'undefined') {
      console.log('nextProps editor content', nextProps.content.content);
      this.editorState = nextProps.content.content;
      editor.trigger.editable.update(nextProps.content.content);
    }
  }
	saveEditorState = () => {
	  const { page, component, call } = this.props;
	  call(
	    'save.editorState',
	    page,
	    component,
	    this.editorState,
	    (error, result) => {
	      if (error) {
	        console.log(error);
	      }
	      console.log(result);
	    }
	  );
	};
	render() {
	  return (
  <StyledEditor>
    <button onClick={this.saveEditorState}>Save</button>
    <Editable
      editor={editor}
      id={this.editorState.id}
      onChange={state => (this.editorState = state)}
    />
    <Trash editor={editor} />
    <DisplayModeToggle editor={editor} />
    <Toolbar editor={editor} />
    <div style={{ margin: 20, backgroundColor: 'grey', height: 1 }} />
  </StyledEditor>
	  );
	}
}

export default withTracker(({ page, component }) => {
  Meteor.subscribe('pages');
  return {
    content: Pages.findOne({
      page,
      component
    }),
    call: Meteor.call
  };
})(OryEditor);
