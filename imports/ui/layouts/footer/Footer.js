import React from 'react';
import { Meteor } from 'meteor/meteor';
import StyledFooter from './StyledFooter';
import StyledContent from './StyledContent';

const Footer = () => {
  if (!Meteor.userId()) return null;

  return (
    <StyledFooter>
      <StyledContent>
        <div>
          <h4>UNTERNEHMEN</h4>
          <a href="/">Werte</a>
          <a href="/">Geschichte</a>
          <a href="/">Partner</a>
          <a href="/">Expansion</a>
        </div>
        <div>
          <h4>JOBS </h4>
          <a href="/">Ausbildung </a>
          <a href="/">Offene Stellen</a>
        </div>
        <div>
          <h4>KONTAKT</h4>
          <a href="/">Stundenpläne</a>
          <a href="/">Standorte</a>
        </div>
      </StyledContent>

      <p>(c) update Fitness 2015</p>
    </StyledFooter>
  );
};

export default Footer;
