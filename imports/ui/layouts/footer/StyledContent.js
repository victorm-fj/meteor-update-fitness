import styled from 'styled-components';

const StyledContent = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;

  & > div {
    flex: 1;
    flex-basis: 15rem;
    color: #fafafa;
    height: 12rem;

    & > a {
      color: #fafafa;
      display: block;
      font-size: 1.2rem;
    }
  }

  & > div:first-child {
    flex-basis: 20rem;
  }
`;

export default StyledContent;
