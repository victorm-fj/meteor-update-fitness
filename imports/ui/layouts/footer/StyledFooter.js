import styled from 'styled-components';

const StyledFooter = styled.div`
  background: ${props => props.theme.darkBackground};
  padding: 4rem 4rem 0;

  & > p {
    color: ${props => props.theme.white};
    text-align: center;
    font-size: 1.2rem;
  }
`;

export default StyledFooter;
