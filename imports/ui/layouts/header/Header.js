import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';

import SocialHeader from './social/SocialHeader';
import NavBarHeader from './navbar/NavBarHeader';
import AdminHeader from './admin/AdminHeader';

const Header = ({ user }) => {
  if (!user) return null;

  return (
    <div>
      <AdminHeader user={user} />
      <SocialHeader />
      <NavBarHeader />
    </div>
  );
};

Header.propTypes = {
  user: PropTypes.object,
};

export default withTracker(() => ({ user: Meteor.user() }))(Header);
