import React from 'react';
import { Accounts } from 'meteor/accounts-base';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { HTTP } from 'meteor/http';

import { deployGatsbySite } from '../../../../api/gatsby';
import StyledAdminHeader from './StyledAdminHeader';
import ChangePasswordModal from './changePassword/ChangePasswordModal';
import UsersManager from './usersManager/UsersManager';
import UploadImagesModal from '../../../components/uploadImages/UploadImagesModal';

class AdminHeader extends React.Component {
	static propTypes = {
		match: PropTypes.object.isRequired,
		location: PropTypes.object.isRequired,
		history: PropTypes.object.isRequired,
	};
	state = {
		syncing: false,
		isPasswordOpen: false,
		isUsersOpen: false,
		isImageUploaderOpen: false,
		deploying: false,
	};
	toggleImageUploader = () =>
		this.setState(prevState => ({
			isImageUploaderOpen: !prevState.isImageUploaderOpen,
		}));
	onLogout = () => {
		Accounts.logout(() => this.props.history.replace('/login'));
	};
	onSyncLocations = () => {
		this.setState({ syncing: true });
		Meteor.call('standortes.sync', (err, result) => {
			if (result) this.setState({ syncing: false });
			else console.log('Something went wrong!');
		});
	};
	onSyncCourses = () => {
		this.setState({ syncing: true });
		Meteor.call('courses.sync', (err, result) => {
			if (result) this.setState({ syncing: false });
			else console.log('Something went wrong!');
		});
	};
	onSyncDates = () => {
		this.setState({ syncing: true });
		Meteor.call('dates.sync', (err, result) => {
			if (result) this.setState({ syncing: false });
			else console.log('Something went wrong!');
		});
	};
	togglePasswordModal = () => {
		this.setState({ isPasswordOpen: !this.state.isPasswordOpen });
	};
	toggleUsersModal = () => {
		this.setState({ isUsersOpen: !this.state.isUsersOpen });
	};
	uploadImage = () => {
		this.toggleImageUploader();
	};
	deployGatsby = async () => {
		this.setState({ deploying: true });
		try {
			await deployGatsbySite();
		} catch (error) {
			console.log('error deploying gatsby site', error);
		}
		this.setState({ deploying: false });
	};
	renderUsersManager() {
		if (this.props.user.username === 'root') {
			return <button onClick={this.toggleUsersModal}>Manage users</button>;
		}
		return null;
	}
	render() {
		const {
			isPasswordOpen,
			isUsersOpen,
			syncing,
			isImageUploaderOpen,
			deploying,
		} = this.state;
		return (
			<StyledAdminHeader>
				<ChangePasswordModal
					isOpen={isPasswordOpen}
					toggleModal={this.togglePasswordModal}
				/>
				<UsersManager
					isOpen={isUsersOpen}
					toggleModal={this.toggleUsersModal}
				/>
				<button onClick={this.deployGatsby}>
					{deploying ? 'Depoying...' : 'Deploy static site'}
				</button>
				<button onClick={this.uploadImage}>Upload image</button>
				{this.renderUsersManager()}
				<button onClick={this.togglePasswordModal}>Change password</button>
				<button onClick={this.onSyncLocations} disabled={syncing}>
					{syncing ? 'Syncing...' : 'Sync locations'}
				</button>
				<button onClick={this.onSyncCourses} disabled={syncing}>
					{syncing ? 'Syncing...' : 'Sync courses'}
				</button>
				<button onClick={this.onSyncDates} disabled={syncing}>
					{syncing ? 'Syncing...' : 'Sync courses dates'}
				</button>
				<button onClick={this.onLogout}>Logout</button>
				<UploadImagesModal
					isOpen={isImageUploaderOpen}
					toggleModal={this.toggleImageUploader}
					page=""
					component=""
					returnURL
				/>
			</StyledAdminHeader>
		);
	}
}

AdminHeader.propTypes = {
	user: PropTypes.object,
};
export default withRouter(AdminHeader);
