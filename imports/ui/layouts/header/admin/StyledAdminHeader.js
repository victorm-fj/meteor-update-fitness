import styled from 'styled-components';

const StyledAdminHeader = styled.div`
  display: flex;
  justify-content: flex-end;
  background: ${props => props.theme.gray};
  padding: 1rem;

  button {
    padding: 0.7rem;
    margin-right: 1rem;
    border: none;
    cursor: pointer;
    outline: none;
    background: none;
    text-decoration: underline;
  }
`;

export default StyledAdminHeader;
