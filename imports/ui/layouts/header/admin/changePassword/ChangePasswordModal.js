import React from 'react';
import Modal from 'react-modal';
import { Accounts } from 'meteor/accounts-base';
import PropTypes from 'prop-types';

import StyledPasswordModal from './StyledPasswordModal';

const ChangePasswordModal = class ChangePasswordModal extends React.Component {
  state = {
    currentPassword: '',
    newPassword: '',
    passwordError: '',
  };

  onSubmitPassword = e => {
    e.preventDefault();
    const { currentPassword, newPassword } = this.state;
    Accounts.changePassword(currentPassword, newPassword, err => {
      if (!err) {
        this.clearState();
        this.props.toggleModal();
      } else this.setState({ passwordError: err.reason });
    });
  };

  onChangeCurrentPassword = e => {
    this.setState({ currentPassword: e.target.value });
  };

  onChangeNewPassword = e => {
    this.setState({ newPassword: e.target.value });
  };

  onRequestClose = () => {
    this.clearState();
    this.props.toggleModal();
  };

  clearState() {
    this.setState({ currentPassword: '', newPassword: '', passwordError: '' });
  }

  render() {
    return (
      <StyledPasswordModal>
        <Modal
          isOpen={this.props.isOpen}
          contentLabel="Change password"
          onAfterOpen={() => this.input.focus()}
          onRequestClose={this.onRequestClose}
          className="password-modal__box"
          overlayClassName="password-modal"
        >
          <form
            onSubmit={this.onSubmitPassword}
            className="password-modal__form"
          >
            <h1>Change your password</h1>

            {this.state.passwordError && <p>{this.state.passwordError}</p>}

            <input
              className="password-modal__input"
              type="password"
              placeholder="Enter current password"
              ref={input => (this.input = input)}
              value={this.state.currentPassword}
              onChange={this.onChangeCurrentPassword}
            />

            <input
              className="password-modal__input"
              type="password"
              placeholder="Enter new password"
              value={this.state.newPassword}
              onChange={this.onChangeNewPassword}
            />

            <button className="password-modal__button">Change password</button>
          </form>
        </Modal>
      </StyledPasswordModal>
    );
  }
};

ChangePasswordModal.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
};

export default ChangePasswordModal;
