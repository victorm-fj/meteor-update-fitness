import styled from 'styled-components';

const StyledPasswordModal = styled.div`
  display: flex;
  justify-content: flex-end;
  background: #ddd;
  padding: 1rem 1.0875rem;

  button {
    padding: 0.7rem;
    margin-right: 1rem;
    border: none;
    cursor: pointer;
    outline: none;
    background: none;
    text-decoration: underline;
  }
`;

export default StyledPasswordModal;
