import styled from 'styled-components';

const StyledUsersManager = styled.div`
  display: flex;
  background: #ddd;
  padding: 1rem 1.0875rem;

  form > div {
    display: flex;
    justify-content: space-between;
  }
`;

export default StyledUsersManager;
