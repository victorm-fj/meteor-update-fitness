import React from 'react';
import Modal from 'react-modal';
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';

import StyledUsersManager from './StyledUsersManager';
import StyledButton from '../../../../components/StyledButton';

class UsersManager extends React.Component {
  state = {
    username: '',
    password: '',
    createError: '',
    deleteError: '',
  };

  onSubmit = e => {
    e.preventDefault();
    const { username, password } = this.state;
    this.props.call('users.create', username, password, (err, result) => {
      if (result) {
        this.clearState();
      } else this.setState({ createError: err.reason });
    });
  };

  onChangeUsername = e => {
    this.setState({ username: e.target.value.trim() });
  };

  onChangePassword = e => {
    this.setState({ password: e.target.value });
  };

  onRequestClose = () => {
    this.clearState();
    this.props.toggleModal();
  };

  clearState() {
    this.setState({
      username: '',
      password: '',
      createError: '',
      deleteError: '',
    });
  }

  deleteUser = userId => {
    this.props.call('users.delete', userId, err => {
      this.setState({ deleteError: err.reason });
    });
  };

  render() {
    return (
      <StyledUsersManager>
        <Modal
          isOpen={this.props.isOpen}
          contentLabel="Users Manager"
          onAfterOpen={() => this.input.focus()}
          onRequestClose={this.onRequestClose}
          className="users-manager-modal__box"
          overlayClassName="password-modal"
        >
          <h1>Create admin user</h1>

          <form onSubmit={this.onSubmit} className="password-modal__form">
            {this.state.createError && <p>{this.state.createError}</p>}

            <div>
              <input
                className="users-manager-modal__input"
                type="text"
                placeholder="Username"
                ref={input => (this.input = input)}
                value={this.state.username}
                onChange={this.onChangeUsername}
              />

              <input
                className="users-manager-modal__input"
                type="password"
                placeholder="Password"
                value={this.state.password}
                onChange={this.onChangePassword}
              />

              <button className="password-modal__button">Create user</button>
            </div>
          </form>

          <h1 style={{ marginTop: '2rem' }}>Manager users</h1>

          {this.state.deleteError && (
            <p style={{ textAlign: 'center' }}>{this.state.deleteError}</p>
          )}

          {this.props.users
            .filter(user => user.username !== 'root')
            .map(user => {
              return (
                <div
                  key={user._id}
                  style={{ display: 'flex', justifyContent: 'space-between' }}
                >
                  <p>{user.username}</p>
                  <StyledButton onClick={() => this.deleteUser(user._id)}>
                    Delete user
                  </StyledButton>
                </div>
              );
            })}
        </Modal>
      </StyledUsersManager>
    );
  }
}

UsersManager.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  users: PropTypes.array.isRequired,
  call: PropTypes.func.isRequired,
};

export default withTracker(() => {
  Meteor.subscribe('users');

  return {
    users: Meteor.users.find().fetch(),
    call: Meteor.call,
  };
})(UsersManager);
