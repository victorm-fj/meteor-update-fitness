import React from 'react';
import { Link } from 'react-router-dom';
import { UnmountClosed } from 'react-collapse';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';

import { Images } from '../../../../api/images';
import DesktopNav from './desktop/DesktopNav';
import MobileNav from './mobile/MobileNav';
import UploadImagesModal from '../../../components/uploadImages/UploadImagesModal';
import EditButton from '../../../components/editButton/EditButton';
import StyledHamburger from './StyledHamburger';
import StyledNavBar from './StyledNavBar';

class NavBarHeader extends React.Component {
  state = { open: false, editable: false };

  onToggle = () => {
    this.setState({ open: !this.state.open });
  };

  toggleEdit = () => {
    this.setState({ editable: !this.state.editable });
  };

  render() {
    return (
      <StyledNavBar>
        <div style={{ flex: 1, alignSelf: 'flex-end' }}>
          <UnmountClosed isOpened={this.state.open} fixedHeight={230}>
            <MobileNav onToggle={this.onToggle} />
          </UnmountClosed>

          <DesktopNav />

          <StyledHamburger
            className={this.state.open ? 'open' : null}
            onClick={this.onToggle}
          />
        </div>

        <div style={{ textAlign: 'right' }}>
          <UploadImagesModal
            isOpen={this.state.editable}
            toggleModal={this.toggleEdit}
            page="home"
            component="navbar"
          />

          <EditButton onClick={this.toggleEdit} />

          <Link to="/">
            {this.props.logo && (
              <img
                src={this.props.logo.imageUrl}
                className="logo"
                alt="update Fitness logo"
              />
            )}
          </Link>
        </div>
      </StyledNavBar>
    );
  }
}

NavBarHeader.propTypes = {
  logo: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    imageId: PropTypes.string,
    imageUrl: PropTypes.string,
    updatedAt: PropTypes.number,
  }),
};

export default withTracker(() => {
  Meteor.subscribe('images');

  return {
    logo: Images.findOne({ page: 'home', component: 'navbar' }),
  };
})(NavBarHeader);
