import styled from 'styled-components';

const StyledNavBar = styled.nav`
  display: flex;
  justify-content: space-between;
  padding: 1rem 1.0875rem 1.45rem;
  position: relative;

  .logo {
    width: 200px;
    margin-right: 1rem;

    @media (max-width: 400px) {
      width: 160px;
    }
  }
`;

export default StyledNavBar;
