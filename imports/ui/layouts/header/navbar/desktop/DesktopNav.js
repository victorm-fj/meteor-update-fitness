import React from 'react';
import { NavLink } from 'react-router-dom';

import StyledDesktopNav from './StyledDesktopNav';

const DesktopNav = () => (
  <StyledDesktopNav>
    <li>
      <NavLink to="/standorte">STANDORTE</NavLink>
    </li>
    <li>
      <NavLink to="/angebote">ANGEBOTE</NavLink>
    </li>
    <li>
      <NavLink to="/probetraining">PROBETRAINING</NavLink>
    </li>
    <li>
      <NavLink to="/mitglied">MITGLIED WERDEN</NavLink>
    </li>
  </StyledDesktopNav>
);

export default DesktopNav;
