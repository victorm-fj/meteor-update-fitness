import styled from 'styled-components';

const StyledDektopNav = styled.ul`
  list-style: none;
  display: flex;
  align-items: flex-end;

  li {
    padding-right: 30px;
    font-weight: 800;
    margin: 0;
    margin-bottom: -0.25rem;

    & > a {
      font-size: 1.6rem;
    }
  }

  @media (max-width: 900px) {
    display: none;
  }
`;

export default StyledDektopNav;
