import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import StyledMobileNav from './StyledMobileNav';

const MobileNav = ({ onToggle }) => (
  <StyledMobileNav>
    <li>
      <NavLink to="/standorte" onClick={onToggle}>
				STANDORTE
      </NavLink>
    </li>

    <li>
      <NavLink to="/angebote" onClick={onToggle}>
				ANGEBOTE
      </NavLink>
    </li>

    <li>
      <NavLink to="/probetraining" onClick={onToggle}>
				PROBETRAINING
      </NavLink>
    </li>

    <li>
      <NavLink to="/mitglied" onClick={onToggle}>
				MITGLIED WERDEN
      </NavLink>
    </li>
  </StyledMobileNav>
);

MobileNav.propTypes = {
  onToggle: PropTypes.func.isRequired
};

export default MobileNav;
