import React from 'react';
import FaSearch from 'react-icons/lib/fa/search';

import StyledSearchHeader from './StyledSearchHeader';

const SearchHeader = () => (
  <StyledSearchHeader>
    <input
      type="text"
      placeholder="Lekon finden - update in meiner Nähe"
      style={{
        background: '#777',
        border: 'none',
        outline: 'none',
        color: '#fafafa',
        padding: '10px',
        marginRight: '10px',
        width: '25rem',
      }}
    />

    <button onClick={() => {}}>
      <FaSearch size={22} color="#fafafa" />
    </button>
  </StyledSearchHeader>
);

export default SearchHeader;
