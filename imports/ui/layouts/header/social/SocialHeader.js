import React from 'react';
import FaFacebook from 'react-icons/lib/fa/facebook';
import FaTwitter from 'react-icons/lib/fa/twitter';
import FaGooglePlus from 'react-icons/lib/fa/google-plus';
import FaLinkedin from 'react-icons/lib/fa/linkedin';
import FaFeed from 'react-icons/lib/fa/feed';
import FaLock from 'react-icons/lib/fa/lock';

import StyledSocialHeader from './StyledSocialHeader';
import SearchHeader from './SearchHeader';

const SocialHeader = () => (
  <StyledSocialHeader>
    <div>
      <button>
        <FaFacebook size={20} color="#333" />
      </button>

      <button>
        <FaTwitter size={20} color="#333" />
      </button>

      <button>
        <FaGooglePlus size={20} color="#333" />
      </button>

      <button>
        <FaLinkedin size={20} color="#333" />
      </button>

      <button>
        <FaFeed size={20} color="#333" />
      </button>

      <button>
        <FaLock size={20} color="#333" />
      </button>
    </div>

    <SearchHeader />
  </StyledSocialHeader>
);

export default SocialHeader;
