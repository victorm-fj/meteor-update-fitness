import styled from 'styled-components';

const StyledSocialHeader = styled.div`
  display: flex;
  justify-content: space-between;
  background: ${props => props.theme.darkBackground};
  padding: 1rem 1.0875rem;

  @media (max-width: 550px) {
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }

  & > div {
    width: 300px;
    display: flex;
    justify-content: space-around;
    align-items: center;

    @media (max-width: 550px) {
      margin-bottom: 1.4rem;
    }
  }

  button {
    width: 30px;
    height: 30px;
    border: none;
    border-radius: 15px;
    background: #999;
    display: flex;
    justify-content: center;
    align-items: center;
    cursor: pointer;
    margin-right: 1rem;
  }
`;

export default StyledSocialHeader;
