import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';

import { Pages } from '../../../api/pages';
import EditableText from '../../components/editableText/EditableText';
import StyledAngebote from './StyledAngebote';
import Editor from '../../editor';

const page = 'angebote';
const component = 'standard_page';

class Angebote extends React.Component {
	state = { editable: false };
	toggleEditor = () => {
	  this.setState({ editable: !this.state.editable });
	};
	render() {
	  return (
      <StyledAngebote>
        <Editor page={page} component="main" />
        <div style={{ width: '100%' }}>
          <EditableText
            editable={this.state.editable}
            toggleEditor={this.toggleEditor}
            page={page}
            component={component}
            editableContent={this.props.editableContent}
            call={this.props.call}
            complex
          />
        </div>
      </StyledAngebote>
	  );
	}
}

Angebote.propTypes = {
  editableContent: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    htmlContent: PropTypes.string,
    updatedAt: PropTypes.number
  }),
  call: PropTypes.func.isRequired
};

export default withTracker(() => {
  Meteor.subscribe('pages');
  return {
    editableContent: Pages.findOne({
      page,
      component
    }),
    call: Meteor.call
  };
})(Angebote);
