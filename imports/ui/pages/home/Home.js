import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import { News } from '../../../api/news';
import { Images } from '../../../api/images';
import UploadImagesSlider from '../../components/uploadImagesSlider/UploadImagesSlider';
import Probetraining from './probetraining/Probetraining';
import EServices from './eServices/EServices';
import UpdateErfolge from './updateErfolge/UpdateErfolge';
import Statistics from './statistics/Statistics';
import StyledHome from './StyledHome';

class Home extends React.Component {
	state = { editable: false, editableNews: false };
	toggleEdit = () => {
		this.setState({ editable: !this.state.editable });
	};
	toggleEditNews = () => {
		this.setState({ editableNews: !this.state.editableNews });
	};
	render() {
		const { editable, editableNews } = this.state;
		const { images, news } = this.props;
		return (
			<StyledHome>
				<div style={{ position: 'relative' }}>
					<UploadImagesSlider
						page="home"
						component="slider"
						editable={editable}
						toggleEdit={this.toggleEdit}
						images={images}
						news={news}
						editableNews={editableNews}
						toggleEditNews={this.toggleEditNews}
					/>
				</div>
				<div className="news">
					<a href="/">alle News</a>
				</div>
				<div className="content">
					<Probetraining />
					<EServices />
					<UpdateErfolge />
					<Statistics />
				</div>
			</StyledHome>
		);
	}
}

Home.propTypes = {
	images: PropTypes.arrayOf(PropTypes.object),
	news: PropTypes.arrayOf(PropTypes.object),
};
export default withTracker(() => {
	Meteor.subscribe('images');
	Meteor.subscribe('news');
	const data = Images.findOne({ page: 'home', component: 'slider' });
	const images = [];
	let news = [];
	if (data) {
		news = News.find(
			{ location: 'home' },
			{ sort: { order: -1, updatedAt: -1 } }
		).fetch();
		console.log('News', news);
		data.sliders.forEach((slider, index) => {
			if (news[index]) {
				images.push({
					imgSrc: slider.imageUrl,
					imgAlt: 'Slider image',
					newsTitle: news[index].title,
					newsBody: news[index].body,
				});
			} else {
				images.push({
					imgSrc: slider.imageUrl,
					imgAlt: 'Slider image',
					newsTitle: '',
					newsBody: '',
				});
			}
		});
	}
	return { images, news };
})(Home);
