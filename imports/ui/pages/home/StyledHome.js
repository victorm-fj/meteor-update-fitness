import styled from 'styled-components';

const StyledHome = styled.div`
  .news {
    ${'' /* padding-left: 1rem; */}

    a {
      display: flex;
      align-items: center;
      font-size: 1.8rem;
    }

    a:before {
      content: '>';
      color: ${props => props.theme.brand};
      font-weight: 800;
      font-size: 2.4rem;
      margin-right: 0.5rem;
    }
  }

  .content {
    width: 80%;
    margin: 0 auto;
  }
`;

export default StyledHome;
