import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';

import { Pages } from '../../../../api/pages';
import { Images } from '../../../../api/images';
import EditableText from '../../../components/editableText/EditableText';
import EditableImage from '../../../components/editableImage/EditableImage';
import StyledEServices from './StyledEServices';

const page = 'home';
const component = 'services';

class EServices extends React.Component {
  state = { editable: false, editImage: false };

  toggleEditor = () => {
    this.setState({ editable: !this.state.editable });
  };

  toggleEditImage = () => {
    this.setState({ editImage: !this.state.editImage });
  };

  render() {
    return (
      <section>
        <StyledEServices
          style={{ flexDirection: this.state.editable ? 'column' : 'row' }}
        >
          <div style={{ flex: 0.4 }}>
            <EditableImage
              page={page}
              component={component}
              alt="E Services"
              editImage={this.state.editImage}
              toggleEditImage={this.toggleEditImage}
              image={this.props.image}
            />
          </div>

          <div style={{ flex: 0.6, marginLeft: '1rem' }}>
            <EditableText
              editable={this.state.editable}
              toggleEditor={this.toggleEditor}
              page={page}
              component={component}
              editableContent={this.props.editableContent}
              call={this.props.call}
            />
          </div>
        </StyledEServices>
      </section>
    );
  }
}

EServices.propTypes = {
  editableContent: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    htmlContent: PropTypes.string,
    updatedAt: PropTypes.number,
  }),
  call: PropTypes.func.isRequired,
  image: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    imageId: PropTypes.string,
    imageUrl: PropTypes.string,
    updatedAt: PropTypes.number,
  }),
};

export default withTracker(() => {
  Meteor.subscribe('pages');
  Meteor.subscribe('images');

  return {
    editableContent: Pages.findOne({
      page,
      component,
    }),
    call: Meteor.call,
    image: Images.findOne({
      page,
      component,
    }),
  };
})(EServices);
