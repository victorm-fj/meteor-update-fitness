import styled from 'styled-components';

const StyledEServices = styled.div`
  display: flex;
  flex-wrap: wrap;

  & > div {
    width: 100%;
    margin: 0 auto;
  }

  & > div:first-child {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    img {
      width: 25rem;
    }
  }

  ul {
    column-count: 2;
  }

  li {
    display: flex;
    align-items: center;
  }

  li:before {
    content: url('img/e-list.png');
  }

  @media (max-width: 1200px) {
    flex-direction: column;

    & > div:first-child {
      margin-bottom: 4rem;
    }
  }

  @media (max-width: 750px) {
    ul {
      column-count: 1;
    }
  }
`;

export default StyledEServices;
