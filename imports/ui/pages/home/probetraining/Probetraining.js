import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';

import { Pages } from '../../../../api/pages';
import StyledProbetraining from './StyledProbetraining';
import EditableText from '../../../components/editableText/EditableText';
import EditableVideo from '../../../components/editableVideo/EditableVideo';

const page = 'home';
const component = 'probetraining';

class Probetraining extends React.Component {
  state = {
    editable: false,
  };

  toggleEditor = () => {
    this.setState({ editable: !this.state.editable });
  };

  render() {
    return (
      <section>
        <StyledProbetraining
          style={{ flexDirection: this.state.editable ? 'column' : 'row' }}
        >
          <div style={{ flex: 0.35 }}>
            <EditableText
              editable={this.state.editable}
              toggleEditor={this.toggleEditor}
              page={page}
              component={component}
              editableContent={this.props.editableContent}
              call={this.props.call}
            />

            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                marginTop: '4rem',
              }}
            >
              <p>
                <a className="probetraining-link" href="/">
                  Online-Anmeldung zum kostenlosen und unverbindlichen
                  Probetraining
                </a>
              </p>
            </div>
          </div>

          <div style={{ flex: 0.15 }} />

          <div style={{ flex: 0.5 }}>
            <EditableVideo
              call={this.props.call}
              page={page}
              component={component}
              editableContent={this.props.editableContent}
            />
          </div>
        </StyledProbetraining>
      </section>
    );
  }
}

Probetraining.propTypes = {
  editableContent: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    htmlContent: PropTypes.string,
    updatedAt: PropTypes.number,
    link: PropTypes.string,
  }),
  call: PropTypes.func.isRequired,
};

export default withTracker(() => {
  Meteor.subscribe('pages');

  return {
    editableContent: Pages.findOne({
      page,
      component,
    }),
    call: Meteor.call,
  };
})(Probetraining);
