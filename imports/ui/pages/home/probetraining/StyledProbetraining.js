import styled from 'styled-components';

const StyledProbetraining = styled.div`
  margin: 4rem auto;
  background: #edf3f7;
  box-shadow: 0 0 0.5rem ${props => props.theme.boxShadow};
  padding: 4rem;
  display: flex;

  .probetraining-link {
    display: flex;
    align-items: center;
  }

  .probetraining-link:before {
    content: '>';
    color: ${props => props.theme.brand};
    font-weight: 800;
    font-size: 2.4rem;
    margin-right: 0.5rem;
  }

  input {
    padding: 0.5rem;
    width: 80%;
  }

  .actions {
    display: flex;
    justify-content: flex-end;
  }

  @media (max-width: 800px) {
    flex-direction: column;
  }

  & > div:first-child > div {
    p {
      font-size: 1.6rem;
    }
  }
`;

export default StyledProbetraining;
