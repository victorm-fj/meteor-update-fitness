import styled from 'styled-components';

const StyledStatistics = styled.div`
  display: flex;

  & > div:last-child {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;

    img {
      width: 22rem;
      height: 22rem;
    }
  }

  ul {
    list-style: disc;
    padding: 2rem;
  }

  @media (max-width: 720px) {
    flex-direction: column;
  }
`;

export default StyledStatistics;
