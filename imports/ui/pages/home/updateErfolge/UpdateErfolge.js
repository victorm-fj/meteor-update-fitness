import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';

import { Pages } from '../../../../api/pages';
import { Images } from '../../../../api/images';
import EditableText from '../../../components/editableText/EditableText';
import EditableImage from '../../../components/editableImage/EditableImage';
import StyledUpdateErfolge from './StyledUpdateErfolge';

const page = 'home';
const component = 'update';

class UpdateErfolge extends React.Component {
  state = { editable: false, editImage: false };

  toggleEditor = () => {
    this.setState({ editable: !this.state.editable });
  };

  toggleEditImage = () => {
    this.setState({ editImage: !this.state.editImage });
  };

  render() {
    return (
      <section>
        <StyledUpdateErfolge
          style={{ flexDirection: this.state.editable ? 'column' : 'row' }}
        >
          <div
            style={{ flex: 0.4, paddingBottom: this.state.editable && '4rem' }}
          >
            <EditableImage
              page={page}
              component={component}
              alt="Update Erfolge"
              editImage={this.state.editImage}
              toggleEditImage={this.toggleEditImage}
              image={this.props.image}
            />
          </div>

          <div style={{ flex: 0.1 }} />

          <div style={{ flex: 0.5 }}>
            <EditableText
              editable={this.state.editable}
              toggleEditor={this.toggleEditor}
              page={page}
              component={component}
              editableContent={this.props.editableContent}
              call={this.props.call}
            />

            <div
              style={{
                display: 'flex',
                alignItems: 'center',
                marginTop: '4rem',
              }}
            >
              <p>
                <a className="success-link" href="/">
                  Poste uns deinen persönlichen Erfolg!
                </a>
              </p>
            </div>
          </div>
        </StyledUpdateErfolge>
      </section>
    );
  }
}

UpdateErfolge.propTypes = {
  editableContent: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    htmlContent: PropTypes.string,
    updatedAt: PropTypes.number,
  }),
  call: PropTypes.func.isRequired,
  image: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    imageId: PropTypes.string,
    imageUrl: PropTypes.string,
    updatedAt: PropTypes.number,
  }),
};

export default withTracker(() => {
  Meteor.subscribe('pages');
  Meteor.subscribe('images');

  return {
    editableContent: Pages.findOne({
      page,
      component,
    }),
    call: Meteor.call,
    image: Images.findOne({
      page,
      component,
    }),
  };
})(UpdateErfolge);
