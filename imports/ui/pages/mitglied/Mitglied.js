import React from 'react';
import StyledMitglied from './StyledMitglied';
import Editor from '../../editor';

const page = 'mitglied';
const component = 'main';

class Mitglied extends React.Component {
    render() {
        return (
            <StyledMitglied>
                <Editor page={page} component={component} />
            </StyledMitglied>
        );
    }
}

export default Mitglied;
