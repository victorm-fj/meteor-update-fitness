import React from 'react';
import StyledProbetraining from './StyledProbetraining';
import Editor from '../../editor';

const page = 'probetraining';
const component = 'main';

class Probetraining extends React.Component {
    render() {
        return (
            <StyledProbetraining>
                <Editor page={page} component={component} />
            </StyledProbetraining>
        );
    }
}

export default Probetraining;
