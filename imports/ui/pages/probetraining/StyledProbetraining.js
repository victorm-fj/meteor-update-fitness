import styled from 'styled-components';

const StyledProbetraining = styled.div`
    width: 80%;
	height: 100%;
	margin: 8rem auto;
	iframe {
		width: 100%;
		height: 400px;
		border: none;
	}
`;

export default StyledProbetraining;
