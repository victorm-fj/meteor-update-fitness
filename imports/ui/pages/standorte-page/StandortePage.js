import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';

import { Dates } from '../../../api/dates';
import { Courses } from '../../../api/courses';
import { News } from '../../../api/news';
import { Images } from '../../../api/images';
import { Standortes } from '../../../api/standortes';
import UploadImagesSlider from '../../components/uploadImagesSlider/UploadImagesSlider';
import EServices from './eServices/EServices';
import Contact from './contact/Contact';
import CoursesSection from './courses/Courses';
import Fitness from './fitness/Fitness';
import Schedule from './schedule/Schedule';
import StyledStandortePage from './StyledStandortePage';
// import { rangeWeek } from './helper';

class StandortePage extends React.Component {
  state = { editable: false, editableNews: false };

  toggleEdit = () => {
    this.setState({ editable: !this.state.editable });
  };

  toggleEditNews = () => {
    this.setState({ editableNews: !this.state.editableNews });
  };

  render() {
    const { match, standorte, images, news, courses } = this.props;
    const page = match.params.page.split('-').join(' ');
    const { editable, editableNews } = this.state;
    console.log(courses);

    return (
      <div>
        <div style={{ position: 'relative' }}>
          <UploadImagesSlider
            page={page}
            component="slider"
            editable={editable}
            toggleEdit={this.toggleEdit}
            images={images}
            news={news}
            editableNews={editableNews}
            toggleEditNews={this.toggleEditNews}
          />
        </div>

        <StyledStandortePage>
          <EServices page={page} />

          <Contact standorte={standorte} page={page} />

          <CoursesSection page={page} courses={courses} />

          <Fitness page={page} />

          <Schedule page={page} />
        </StyledStandortePage>
      </div>
    );
  }
}

StandortePage.propTypes = {
  courses: PropTypes.array,
  standorte: PropTypes.object,
  match: PropTypes.object,
  images: PropTypes.arrayOf(PropTypes.object),
  news: PropTypes.arrayOf(PropTypes.object),
};

export default withTracker(({ match }) => {
  const name = match.params.page.split('-').join(' ');
  Meteor.subscribe('standorte', name);
  Meteor.subscribe('images');
  Meteor.subscribe('news');
  Meteor.subscribe('courses');
  Meteor.subscribe('dates');

  const data = Images.findOne({ page: name, component: 'slider' });
  const images = [];
  let news = [];

  if (data) {
    // if (data.news) {
      // const newsIds = data.news.map(
      //   item => new Meteor.Collection.ObjectID(item.newsId)
      // );
      // find news which ids are in newsIds array
      // and sort them by order, and the by updatedAt fields
      news = News.find(
        // { _id: { $in: newsIds } },
        { location: name },
        { sort: { order: -1, updatedAt: -1 } }
      ).fetch();
      console.log('News', news);

      data.sliders.forEach((slider, index) => {
        if (news[index]) {
          images.push({
            imgSrc: slider.imageUrl,
            imgAlt: 'Slider image',
            newsTitle: news[index].title,
            newsBody: news[index].body,
          });
        } else {
          images.push({
            imgSrc: slider.imageUrl,
            imgAlt: 'Slider image',
            newsTitle: '',
            newsBody: '',
          });
        }
      });
    // } else {
    //   data.sliders.forEach(slider => {
    //     images.push({
    //       imgSrc: slider.imageUrl,
    //       imgAlt: 'Slider image',
    //       newsTitle: '',
    //       newsBody: '',
    //     });
    //   });
    // }
  }

  const standorte = Standortes.findOne({ name });
  let courses = null;
  let filteredCourses = null;
  if (standorte) {
    courses = Courses.find({ location_name: standorte.name }).fetch();

    if (courses) {
      // const weekRange = rangeWeek();
      // const startDate = weekRange.start.getTime();
      // const endDate = weekRange.end.getTime();

      filteredCourses = courses.map(course => {
        const query = {
          // startdate: { $gte: startDate, $lt: endDate },
          id_course: course.id,
        };
        const startdate = Dates.findOne(query);

        return { ...course, startdate };
      });
    }
  }

  return {
    courses: filteredCourses,
    standorte,
    images,
    news,
  };
})(StandortePage);
