import styled from 'styled-components';

const StyledStandortePage = styled.div`
  width: 80%;
  margin: auto;
`;

export default StyledStandortePage;
