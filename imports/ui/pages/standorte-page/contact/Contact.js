import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';

import { Pages } from '../../../../api/pages';
import EditableText from '../../../components/editableText/EditableText';
import StandorteMap from './StandorteMap';
import StyledContact from './StyledContact';

const component = 'contact';

class Contact extends React.Component {
  state = { editable: false };

  toggleEditor = () => {
    this.setState({ editable: !this.state.editable });
  };

  render() {
    const { standorte } = this.props;

    if (standorte) {
      return (
        <section>
          <StyledContact>
            <div style={{ flex: 0.4 }}>
              <div style={{ position: 'relative' }}>
                <EditableText
                  editable={this.state.editable}
                  toggleEditor={this.toggleEditor}
                  page={this.props.page}
                  component={component}
                  editableContent={this.props.editableContent}
                  call={this.props.call}
                />
              </div>

              <p>update Fitness {standorte.name}</p>
              <p>{standorte.address1}</p>
              <p>
                {standorte.zip} {standorte.city}
              </p>
              <p>Centerleitung: Severin Häring</p>
              <h3>Telefon +41 71 244 83 47</h3>
            </div>

            <div style={{ flex: 0.6 }}>
              <StandorteMap {...standorte} />
            </div>
          </StyledContact>
        </section>
      );
    }

    return null;
  }
}

Contact.propTypes = {
  standorte: PropTypes.object,
};

Contact.propTypes = {
  editableContent: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    htmlContent: PropTypes.string,
    updatedAt: PropTypes.number,
  }),
  call: PropTypes.func.isRequired,
  page: PropTypes.string.isRequired,
};

export default withTracker(({ page }) => {
  Meteor.subscribe('pages');

  return {
    editableContent: Pages.findOne({
      page,
      component,
    }),
    call: Meteor.call,
  };
})(Contact);
