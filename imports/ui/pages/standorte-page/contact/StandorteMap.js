import _ from 'lodash';
import React from 'react';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow,
} from 'react-google-maps';
import FaSpinner from 'react-icons/lib/fa/spinner';

const googleMapURL =
  'https://maps.googleapis.com/maps/api/js?v=3.27&libraries=places,geometry&key=AIzaSyDUjTVjJBiAtMiwq_BRLDkS4NEXBPtqW4E';

const AsyncGoogleMap = withScriptjs(
  withGoogleMap(props => {
    return (
      <GoogleMap
        ref={props.onMapLoad}
        defaultZoom={8}
        defaultCenter={{
          lat: Number(props.lat),
          lng: Number(props.lon),
        }}
        onClick={props.onMapClick}
      >
        {props.markers.map(marker => (
          <Marker
            options={{ icon: '/img/marker.png' }}
            {...marker}
            onRightClick={() => props.onMarkerRightClick(marker)}
            onClick={props.toggleInfoWindow}
          >
            {/* only compare latitude because the value return by clicking
              the marker is exactly the value of the latitude given by the data */}
            {props.windowPosition && (
              <InfoWindow
                position={props.windowPosition}
                onCloseclick={props.toggleInfoWindow}
              >
                <div>
                  <p style={{ margin: '0' }}>
                    Update Fitness {props.name}
                    <br />
                    {props.address1}
                    <br />
                    {props.zip} {props.city}
                  </p>
                </div>
              </InfoWindow>
            )}
          </Marker>
        ))}
      </GoogleMap>
    );
  })
);

class StandorteMap extends React.Component {
  state = {
    markers: [
      {
        position: {
          lat: Number(this.props.lat),
          lng: Number(this.props.lon),
        },
        key: `${this.props.id}`,
        defaultAnimation: 2,
      },
    ],
    windowPosition: null,
  };

  handleMapLoad = map => {
    this._mapComponent = map;
    if (map) {
      console.log(map.getZoom());
    }
  };

  /*
   * This is called when you click on the map.
   * Go and try click now.
   */
  handleMapClick = event => {
    const nextMarkers = [
      ...this.state.markers,
      {
        position: event.latLng,
        defaultAnimation: 2,
        key: Date.now(), // Add a key property for: http://fb.me/react-warning-keys
      },
    ];
    this.setState({
      markers: nextMarkers,
    });

    if (nextMarkers.length === 3) {
      this.props.toast(
        `Right click on the marker to remove it`,
        `Also check the code!`
      );
    }
  };

  handleMarkerRightClick = targetMarker => {
    /*
     * All you modify is data, and the view is driven by data.
     * This is so called data-driven-development. (And yes, it's now in
     * web front end and even with google maps API.)
     */
    const nextMarkers = this.state.markers.filter(
      marker => marker !== targetMarker
    );
    this.setState({
      markers: nextMarkers,
    });
  };

  toggleInfoWindow = loc => {
    // clicking 'x' in the info window will pass null, so if we detect that, reset the position in state
    if (loc == null) {
      this.setState({ windowPosition: null });
      return;
    }
    // otherwise get coords of clicked marker and set to state
    let markerLoc = { lat: loc.latLng.lat(), lng: loc.latLng.lng() };
    this.setState({ windowPosition: markerLoc });
  };

  render() {
    return (
      <div style={{ height: 200 }}>
        <AsyncGoogleMap
          {...this.props}
          googleMapURL={googleMapURL}
          loadingElement={
            <div style={{ height: `100%` }}>
              <FaSpinner
                style={{
                  display: `block`,
                  width: `80px`,
                  height: `80px`,
                  margin: `150px auto`,
                  animation: `fa-spin 2s infinite linear`,
                }}
              />
            </div>
          }
          containerElement={<div style={{ height: `100%` }} />}
          mapElement={<div style={{ height: `100%` }} />}
          onMapLoad={_.noop}
          onMapClick={_.noop}
          markers={this.state.markers}
          onMarkerRightClick={_.noop}
          toggleInfoWindow={this.toggleInfoWindow}
          windowPosition={this.state.windowPosition}
        />
      </div>
    );
  }
}

export default StandorteMap;
