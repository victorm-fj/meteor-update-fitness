import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';

import { Pages } from '../../../../api/pages';
import EditableText from '../../../components/editableText/EditableText';
import StyledCourses from './StyledCourses';
import CoursesModal from './modal/CoursesModal';

const component = 'courses';

class Courses extends React.Component {
  state = { editable: false, isOpen: false };

  toggleEditor = () => {
    this.setState({ editable: !this.state.editable });
  };

  toggleModal = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  render() {
    return (
      <section>
        <div style={{ position: 'relative' }}>
          <EditableText
            editable={this.state.editable}
            toggleEditor={this.toggleEditor}
            page={this.props.page}
            component={component}
            editableContent={this.props.editableContent}
            call={this.props.call}
          />
        </div>

        <StyledCourses>
          <div style={{ flex: 0.7 }}>
            <div className="course">
              <p>Donnerstag, 5. November 2015 - 18:00</p>
              <p>BBP (Bauch, Beine, Po) / Bodytone Dauer: 55min</p>
            </div>

            <div className="course">
              <p>Donnerstag, 5. November 2015 - 19:00</p>
              <p>P.I.I.T Dauer: 30min</p>
            </div>

            <div className="course">
              <p>Montag, 9. November 2015 - 12:15</p>
              <p>P.I.I.T Dauer: 30min</p>
            </div>

            <div className="course">
              <p>Montag, 9. November 2015 - 18:00</p>
              <p>Tae Bo Dauer: 55min</p>
            </div>
          </div>

          <div style={{ flex: 0.3 }}>
            <button onClick={this.toggleModal}>
              Stundenplan {this.props.page}
            </button>

            <CoursesModal
              isOpen={this.state.isOpen}
              toggleModal={this.toggleModal}
              courses={this.props.courses}
              page={this.props.page}
            />
          </div>
        </StyledCourses>
      </section>
    );
  }
}

Courses.propTypes = {
  editableContent: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    htmlContent: PropTypes.string,
    updatedAt: PropTypes.number,
  }),
  call: PropTypes.func.isRequired,
  page: PropTypes.string.isRequired,
  courses: PropTypes.array,
};

export default withTracker(({ page }) => {
  Meteor.subscribe('pages');

  return {
    editableContent: Pages.findOne({
      page,
      component,
    }),
    call: Meteor.call,
  };
})(Courses);
