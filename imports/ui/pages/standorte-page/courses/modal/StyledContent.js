import styled from 'styled-components';

const StyledContent = styled.div`
  position: relative;
  padding: 8rem;

  button {
    padding: 0.7rem;
    border: none;
    cursor: pointer;
    outline: none;
    background: none;
    position: absolute;
    top: 0;
    right: 0;
  }

  .course-day {
    display: flex;

    h4 {
      margin-right: 1.4rem;
      width: 3rem;
    }

    p {
      font-size: 1.2rem;
      margin-bottom: 1rem;
    }

    span {
      color: #659841;
      font-weight: 600;
    }
  }
`;

export default StyledContent;
