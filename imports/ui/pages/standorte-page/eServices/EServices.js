import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';

import { Pages } from '../../../../api/pages';
import { Images } from '../../../../api/images';
import EditableText from '../../../components/editableText/EditableText';
import StyledEServices from './StyledEServices';

const component = 'services';

class EServices extends React.Component {
  state = { editable: false };

  toggleEditor = () => {
    this.setState({ editable: !this.state.editable });
  };

  render() {
    return (
      <section>
        <StyledEServices>
          <div style={{ position: 'relative' }}>
            <EditableText
              editable={this.state.editable}
              toggleEditor={this.toggleEditor}
              page={this.props.page}
              component={component}
              editableContent={this.props.editableContent}
              call={this.props.call}
            />
          </div>

          <div>
            {this.props.image && (
              <img src={this.props.image.imageUrl} alt="E Services" />
            )}
          </div>
        </StyledEServices>
      </section>
    );
  }
}

EServices.propTypes = {
  editableContent: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    htmlContent: PropTypes.string,
    updatedAt: PropTypes.number,
  }),
  call: PropTypes.func.isRequired,
  page: PropTypes.string.isRequired,
  image: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    imageId: PropTypes.string,
    imageUrl: PropTypes.string,
    updatedAt: PropTypes.number,
  }),
};

export default withTracker(({ page }) => {
  Meteor.subscribe('pages');
  Meteor.subscribe('images');

  return {
    editableContent: Pages.findOne({
      page,
      component,
    }),
    call: Meteor.call,
    image: Images.findOne({
      page: 'home',
      component,
    }),
  };
})(EServices);
