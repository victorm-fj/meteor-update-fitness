import styled from 'styled-components';

const StyledEServices = styled.div`
  display: flex;

  & > div {
    flex: 0.5;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  & > div:last-child {
    img {
      width: 25rem;
    }
  }

  li {
    display: flex;
    align-items: center;
  }

  li:before {
    content: url('https://res.cloudinary.com/devic31/image/upload/v1510547266/e-list_ynfzff.png');
  }

  @media (max-width: 750px) {
    flex-direction: column;

    & > div:last-child {
      order: 1;

      img {
        margin-bottom: 4rem;
      }
    }

    & > div:first-child {
      order: 2;
    }
  }
`;

export default StyledEServices;
