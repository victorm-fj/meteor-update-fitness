import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';

import { Pages } from '../../../../api/pages';
import { Images } from '../../../../api/images';
import EditableText from '../../../components/editableText/EditableText';
import EditableImage from '../../../components/editableImage/EditableImage';
import StyledFitness from './StyledFitness';

const component = 'fitness';

class Fitness extends React.Component {
  state = { editable: false, editImage: false };

  toggleEditor = () => {
    this.setState({ editable: !this.state.editable });
  };

  toggleEditImage = () => {
    this.setState({ editImage: !this.state.editImage });
  };

  render() {
    return (
      <section>
        <StyledFitness>
          <div>
            <EditableImage
              page={this.props.page}
              component={component}
              alt="Standorte Fitness"
              editImage={this.state.editImage}
              toggleEditImage={this.toggleEditImage}
              image={this.props.image}
            />
          </div>

          <div style={{ flex: 0.1 }} />

          <div style={{ position: 'relative' }}>
            <EditableText
              editable={this.state.editable}
              toggleEditor={this.toggleEditor}
              page={this.props.page}
              component={component}
              editableContent={this.props.editableContent}
              call={this.props.call}
            />
          </div>
        </StyledFitness>
      </section>
    );
  }
}

Fitness.propTypes = {
  editableContent: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    htmlContent: PropTypes.string,
    updatedAt: PropTypes.number,
  }),
  call: PropTypes.func.isRequired,
  page: PropTypes.string.isRequired,
  image: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    imageId: PropTypes.string,
    imageUrl: PropTypes.string,
    updatedAt: PropTypes.number,
  }),
};

export default withTracker(({ page }) => {
  Meteor.subscribe('pages');
  Meteor.subscribe('images');

  return {
    editableContent: Pages.findOne({
      page,
      component,
    }),
    call: Meteor.call,
    image: Images.findOne({
      page,
      component,
    }),
  };
})(Fitness);
