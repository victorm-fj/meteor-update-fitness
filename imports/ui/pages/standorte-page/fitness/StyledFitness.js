import styled from 'styled-components';

const StyledFitness = styled.div`
  margin: 4rem auto;
  background: ${props => props.theme.lightBlue};
  box-shadow: 0 0 0.5rem ${props => props.theme.boxShadow};
  padding: 4rem;
  display: flex;

  & > div {
    flex: 0.5;

    p {
      line-height: 1.5;
    }
  }

  & > div:first-child {
    flex: 0.4;
    display: flex;
    justify-content: center;
    align-items: center;

    img {
      width: 25rem;
    }
  }

  @media (max-width: 800px) {
    flex-direction: column;

    & > div:first-child {
      img {
        padding-bottom: 4rem;
      }
    }
  }
`;

export default StyledFitness;
