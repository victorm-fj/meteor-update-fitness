import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';

import { Pages } from '../../../../api/pages';
import EditableText from '../../../components/editableText/EditableText';
import StyledSchedule from './StyledSchedule';

const component = 'schedule';

class Schedule extends React.Component {
  state = { editable: false };

  toggleEditor = () => {
    this.setState({ editable: !this.state.editable });
  };

  render() {
    return (
      <section>
        <StyledSchedule>
          <div style={{ position: 'relative' }}>
            <EditableText
              editable={this.state.editable}
              toggleEditor={this.toggleEditor}
              page={this.props.page}
              component={component}
              editableContent={this.props.editableContent}
              call={this.props.call}
            />
          </div>

          <div>
            <p>Montag, Miwoch,Freitag</p>
            <p>09.00 - 21.30 Uhr</p>
          </div>

          <div>
            <p>Samstag, Sonntag und Feiertag</p>
            <p>06.30 - 21.30 Uhr</p>
          </div>

          <div>
            <p>Samstag, Sonntag und Feiertage</p>
            <p>09.00 - 14.00 Uhr</p>
          </div>
        </StyledSchedule>
      </section>
    );
  }
}

Schedule.propTypes = {
  editableContent: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    htmlContent: PropTypes.string,
    updatedAt: PropTypes.number,
  }),
  call: PropTypes.func.isRequired,
  page: PropTypes.string.isRequired,
};

export default withTracker(({ page }) => {
  Meteor.subscribe('pages');

  return {
    editableContent: Pages.findOne({
      page,
      component,
    }),
    call: Meteor.call,
  };
})(Schedule);
