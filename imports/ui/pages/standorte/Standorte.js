import React from 'react';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';

import MapSection from './map/MapSection';
import SearchResults from './searchResults/SearchResults';
import AlleStandorte from './alleStandorte/AlleStandorte';
import { Standortes } from '../../../api/standortes';
import StyledStandorte from './StyledStandorte';

class Standorte extends React.Component {
  state = {
    searchTerm: '',
    searchResults: [],
    isLoading: false,
    coords: {},
  };

  componentDidMount() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition(
        position => {
          const { latitude, longitude } = position.coords;
          this.setState({ coords: { latitude, longitude } });
        },
        () => {
          alert('Position could not be determined.');
        },
        {
          enableHighAccuracy: true,
        }
      );
    }
  }

  onChangeSearchTerm = e => {
    const searchTerm = e.target.value;
    this.setState({ searchTerm });
  };

  onSubmit = e => {
    e.preventDefault();
    const { searchTerm } = this.state;

    if (searchTerm === '') {
      alert('Please enter a search term.');
    } else {
      this.setState({ isLoading: true });

      this.props.call(
        'standortes.search',
        this.state.searchTerm,
        (err, result) => {
          if (err) {
            alert(err.reason);
          } else if (result.length === 0) {
            this.setState({
              searchResults: [undefined],
              isLoading: false,
            });
          } else {
            this.setState({
              searchResults: result,
              isLoading: false,
            });
          }
        }
      );
    }
  };

  render() {
    const { searchResults, coords } = this.state;
    const userLocation = {
      lat: coords.latitude,
      lon: coords.longitude,
      userLoc: true,
    };

    const locations = [...this.props.locations, userLocation];

    return (
      <div>
        <MapSection
          locations={locations}
          searchTerm={this.state.searchTerm}
          onChange={this.onChangeSearchTerm}
          onSubmit={this.onSubmit}
        />

        <StyledStandorte>
          {searchResults.length > 0 && (
            <SearchResults coords={coords} results={searchResults} />
          )}

          <AlleStandorte locations={this.props.locations} />
        </StyledStandorte>
      </div>
    );
  }
}

Standorte.propTypes = {
  locations: PropTypes.array,
  call: PropTypes.func.isRequired,
};

export default withTracker(() => {
  Meteor.subscribe('standortes');

  return {
    locations: Standortes.find({}, { sort: { name: 1 } }).fetch(),
    call: Meteor.call,
  };
})(Standorte);
