import styled from 'styled-components';

const StyledStandorte = styled.div`
  width: 80%;
  margin: 0 auto;
`;

export default StyledStandorte;
