import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';

import { Pages } from '../../../../api/pages';
import EditableText from '../../../components/editableText/EditableText';
import StyledAlleStandorte from './StyledAlleStandorte';

const page = 'standorte';
const component = 'allestandorte';

class AlleStandorte extends React.Component {
  state = { editable: false };

  toggleEditor = () => {
    this.setState({ editable: !this.state.editable });
  };

  render() {
    return (
      <section>
        <div style={{ position: 'relative' }}>
          <EditableText
            editable={this.state.editable}
            toggleEditor={this.toggleEditor}
            page={page}
            component={component}
            editableContent={this.props.editableContent}
            call={this.props.call}
          />
        </div>

        <StyledAlleStandorte>
          <ul>
            {this.props.locations
              .filter(location => location.lat !== null)
              .map(location => (
                <li key={location.id}>
                  <Link to={`/standorte/${location.name.split(' ').join('-')}`}>
                    {location.name}
                  </Link>
                </li>
              ))}
          </ul>
        </StyledAlleStandorte>
      </section>
    );
  }
}

AlleStandorte.propTypes = {
  locations: PropTypes.array.isRequired,
  editableContent: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    htmlContent: PropTypes.string,
    updatedAt: PropTypes.number,
  }),
  call: PropTypes.func.isRequired,
};

export default withTracker(() => {
  Meteor.subscribe('pages');

  return {
    editableContent: Pages.findOne({
      page,
      component,
    }),
    call: Meteor.call,
  };
})(AlleStandorte);
