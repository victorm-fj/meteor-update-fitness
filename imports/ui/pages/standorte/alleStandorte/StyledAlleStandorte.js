import styled from 'styled-components';

const StyledAlleStandorte = styled.div`
  display: flex;

  ul {
    width: 100%;
    column-count: 3;
  }

  @media (max-width: 600px) {
    ul {
      column-count: 2;
    }
  }
`;

export default StyledAlleStandorte;
