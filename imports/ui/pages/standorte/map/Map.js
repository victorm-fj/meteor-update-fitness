import _ from 'lodash';
import React from 'react';
import { Link } from 'react-router-dom';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow,
} from 'react-google-maps';
import FaSpinner from 'react-icons/lib/fa/spinner';
import FaStreetView from 'react-icons/lib/fa/street-view';
import { MarkerWithLabel } from 'react-google-maps/lib/components/addons/MarkerWithLabel';

const googleMapURL =
  'https://maps.googleapis.com/maps/api/js?v=3.27&libraries=places,geometry&key=AIzaSyDUjTVjJBiAtMiwq_BRLDkS4NEXBPtqW4E';

const AsyncGoogleMap = withScriptjs(
  withGoogleMap(props => {
    return (
      <GoogleMap
        ref={props.onMapLoad}
        defaultZoom={8}
        defaultCenter={{
          lat: 47.4086836,
          lng: 8.5955786,
        }}
        onClick={props.onMapClick}
      >
        {props.markers.map(marker => {
          if (marker.userLoc) {
            return (
              <MarkerWithLabel
                options={{ icon: '/img/user.png' }}
                key={marker.position.lat}
                position={marker.position}
                labelAnchor={new google.maps.Point(0, 0)}
              >
                <div>{/* <FaStreetView color="#088CA4" size={42} /> */}</div>
              </MarkerWithLabel>
            );
          }

          return (
            <Marker
              options={{ icon: '/img/marker.png' }}
              {...marker}
              onRightClick={() => props.onMarkerRightClick(marker)}
              onClick={props.toggleInfoWindow}
            >
              {/* only compare latitude because the value return by clicking
                the marker is exactly the value of the latitude given by the data */}
              {props.windowPosition &&
                props.windowPosition.lat === marker.position.lat && (
                  <InfoWindow
                    position={props.windowPosition}
                    onCloseclick={props.toggleInfoWindow}
                  >
                    <div>
                      <p style={{ margin: '0' }}>
                        Update Fitness{' '}
                        <Link
                          style={{ color: '#bbda0b', fontWeight: 'bold' }}
                          to={`/standorte/${marker.name.split(' ').join('-')}`}
                        >
                          {marker.name}
                        </Link>
                        <br />
                        {marker.address1}
                        <br />
                        {marker.zip} {marker.city}
                      </p>
                    </div>
                  </InfoWindow>
                )}
            </Marker>
          );
        })}
      </GoogleMap>
    );
  })
);

class Map extends React.Component {
  state = {
    // markers: this.props.locations
    //   .filter(({ lat, lon }) => lat !== null && lon !== null)
    //   .map(location => {
    //     return {
    //       position: { lat: Number(location.lat), lng: Number(location.lon) },
    //       key: `${location.id}`,
    //       defaultAnimation: 2,
    //       // label: `${location.name}`,
    //       address1: location.address1,
    //       name: location.name,
    //       zip: location.zip,
    //       city: location.city,
    //     };
    //   }),
    windowPosition: null,
  };

  produceMarkers(locations) {
    return locations
      .filter(({ lat, lon }) => lat !== null && lon !== null)
      .map(location => {
        return {
          position: { lat: Number(location.lat), lng: Number(location.lon) },
          key: `${location.id}`,
          defaultAnimation: 2,
          // label: `${location.name}`,
          address1: location.address1,
          name: location.name,
          zip: location.zip,
          city: location.city,
          userLoc: location.userLoc,
        };
      });
  }

  handleMapLoad = map => {
    this._mapComponent = map;
    if (map) {
      console.log(map.getZoom());
    }
  };

  /*
   * This is called when you click on the map.
   * Go and try click now.
   */
  handleMapClick = event => {
    const nextMarkers = [
      ...this.state.markers,
      {
        position: event.latLng,
        defaultAnimation: 2,
        key: Date.now(), // Add a key property for: http://fb.me/react-warning-keys
      },
    ];
    this.setState({
      markers: nextMarkers,
    });

    if (nextMarkers.length === 3) {
      this.props.toast(
        `Right click on the marker to remove it`,
        `Also check the code!`
      );
    }
  };

  handleMarkerRightClick = targetMarker => {
    /*
     * All you modify is data, and the view is driven by data.
     * This is so called data-driven-development. (And yes, it's now in
     * web front end and even with google maps API.)
     */
    const nextMarkers = this.state.markers.filter(
      marker => marker !== targetMarker
    );
    this.setState({
      markers: nextMarkers,
    });
  };

  toggleInfoWindow = loc => {
    // clicking 'x' in the info window will pass null, so if we detect that, reset the position in state
    if (loc === null) {
      this.setState({ windowPosition: null });
      return;
    }
    // otherwise get coords of clicked marker and set to state
    let markerLoc = { lat: loc.latLng.lat(), lng: loc.latLng.lng() };
    this.setState({ windowPosition: markerLoc });
  };

  render() {
    return (
      <div style={{ height: 350 }}>
        <AsyncGoogleMap
          googleMapURL={googleMapURL}
          loadingElement={
            <div style={{ height: `100%` }}>
              <FaSpinner
                style={{
                  display: `block`,
                  width: `80px`,
                  height: `80px`,
                  margin: `150px auto`,
                  animation: `fa-spin 2s infinite linear`,
                }}
              />
            </div>
          }
          containerElement={<div style={{ height: `100%` }} />}
          mapElement={<div style={{ height: `100%` }} />}
          onMapLoad={_.noop}
          onMapClick={_.noop}
          markers={this.produceMarkers(this.props.locations)}
          onMarkerRightClick={_.noop}
          toggleInfoWindow={this.toggleInfoWindow}
          windowPosition={this.state.windowPosition}
        />
      </div>
    );
  }
}

export default Map;
