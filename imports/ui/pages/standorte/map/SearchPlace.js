import React from 'react';
import PropTypes from 'prop-types';
import FaSearch from 'react-icons/lib/fa/search';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';

import { Pages } from '../../../../api/pages';
import EditableText from '../../../components/editableText/EditableText';
import StyledSearchPlace from './StyledSearchPlace';

const page = 'standorte';
const component = 'search';

class SearchPlace extends React.Component {
  state = { editable: false };

  toggleEditor = () => {
    this.setState({ editable: !this.state.editable });
  };

  render() {
    return (
      <StyledSearchPlace>
        <div style={{ position: 'relative' }}>
          <EditableText
            editable={this.state.editable}
            toggleEditor={this.toggleEditor}
            page={page}
            component={component}
            editableContent={this.props.editableContent}
            call={this.props.call}
          />
        </div>

        <form onSubmit={this.props.onSubmit}>
          <input
            type="text"
            style={{
              background: '#777',
              border: 'none',
              outline: 'none',
              color: '#fafafa',
              padding: '10px',
              marginRight: '10px',
            }}
            value={this.props.searchTerm}
            onChange={this.props.onChange}
          />

          <button type="submit">
            <FaSearch size={24} color="#fafafa" />
          </button>
        </form>
      </StyledSearchPlace>
    );
  }
}

SearchPlace.propTypes = {
  editableContent: PropTypes.shape({
    _id: PropTypes.string,
    page: PropTypes.string,
    component: PropTypes.string,
    htmlContent: PropTypes.string,
    updatedAt: PropTypes.number,
    link: PropTypes.string,
  }),
  call: PropTypes.func.isRequired,
  searchTerm: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default withTracker(() => {
  Meteor.subscribe('pages');

  return {
    editableContent: Pages.findOne({
      page,
      component,
    }),
    call: Meteor.call,
  };
})(SearchPlace);
