import styled from 'styled-components';

const StyledSearchPlace = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 4rem;
  color: #fafafa;

  h2 {
    text-align: center;
  }

  & > form {
    display: flex;
    justify-content: center;
    align-items: center;

    button {
      background: none;
      border: none;
      cursor: pointer;
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }
`;

export default StyledSearchPlace;
