import styled from 'styled-components';

const StyledSearchResults = styled.div`
  margin: 4rem auto;
  background: #edf3f7;
  box-shadow: 0 0 0.5rem ${props => props.theme.boxShadow};
  padding: 4rem;
`;

export default StyledSearchResults;
