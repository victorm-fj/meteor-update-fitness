import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import geolib from 'geolib';

const compare = (a, b) => {
  if (a.distance > b.distance) {
    return 1;
  }

  if (a.distance < b.distance) {
    return -1;
  }

  return 0;
};

const Success = ({ results, coords }) => {
  // some standortes have lat and lon set to null, so
  // first filter the array results
  const standortes = results
    .filter(standorte => !!standorte.lat)
    .map(standorte => {
      const { lat, lon } = standorte;
      let distance;
      let km;

      // user may have not let the webapp known his location
      if (coords.latitude) {
        distance = geolib.getDistance(
          coords,
          { latitude: parseFloat(lat), longitude: parseFloat(lon) },
          100
        );
        km = Math.round(distance / 1000 * 10) / 10;
        standorte.distance = km;
      }

      return standorte;
    });

  if (coords.latitude) {
    standortes.sort(compare);
  }

  return (
    <div>
      <h2>AM NÄCHSTEN ZU DIR</h2>

      <ul style={{ paddingLeft: '1rem' }}>
        {standortes.map(standorte => {
          return (
            <p key={standorte._id}>
              {standorte.distance || '~'} km update {standorte.name},{' '}
              {standorte.address1}, {standorte.zip} {standorte.city}{' '}
              <Link to={`/standorte/${standorte.name.split(' ').join('-')}`}>
                &gt; weitere Informaonen
              </Link>
            </p>
          );
        })}
      </ul>
    </div>
  );
};

Success.propTypes = {
  results: PropTypes.array,
  coords: PropTypes.shape({
    latitude: PropTypes.number,
    longitude: PropTypes.number,
  }),
};

export default Success;
