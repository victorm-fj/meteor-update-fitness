const theme = {
  brand: '#bbda0b',
  white: '#fafafa',
  darkBackground: '#333333',
  editButton: '#8c8c8c',
  gray: '#dddddd',
  socialBackground: '#999999',
  boxShadow: 'rgba(0, 0, 0, 0.5)',
  lightBlue: '#edf3f7',
};

export default theme;
