import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Roles } from 'meteor/alanning:roles';
import { WebApp } from 'meteor/webapp';

import '../imports/api/users';
import '../imports/api/standortes';
import { Courses } from '../imports/api/courses';
import { Dates } from '../imports/api/dates';
import '../imports/startup/simple-schema-config.js';
import '../imports/api/pages';
import '../imports/api/images';
import '../imports/api/news';
import { Status } from '../imports/api/status';

Meteor.startup(() => {
	// Add root 'super-admin' user if not found
	if (Meteor.users.find().count() === 0) {
		const userId = Accounts.createUser({ username: 'root', password: 'root' });
		Roles.addUsersToRoles(userId, ['super-admin']);
		console.log(
			'Creating root account, please login as root and change password.'
		);
	}
	// Add status type 'deploy' if not found
	if (Status.find({ type: 'deploy' }).count() === 0) {
		const doc = { type: 'deploy', status: 'ok' };
		Status.insert(doc);
		console.log('Status record of type deploy created.');
	}
	// REST API endpoint
	const coursesRoute = '/api/courses/';
	WebApp.connectHandlers.use(coursesRoute, (req, res, next) => {
		console.log(req.url);
		const standorteName = req.url
			.split('/')[1]
			.split('-')
			.join(' ');
		const courses = Courses.find({
			location_name: { $regex: new RegExp(standorteName, 'i') },
		});
		let filteredCourses = [];
		if (courses) {
			// const weekRange = rangeWeek();
			// const startDate = weekRange.start.getTime();
			// const endDate = weekRange.end.getTime();

			filteredCourses = courses.map(course => {
				const query = {
					// startdate: { $gte: startDate, $lt: endDate },
					id_course: course.id,
				};
				const startdate = Dates.findOne(query);

				return { ...course, startdate };
			});
		}
		res.setHeader('Access-Control-Allow-Origin', '*');
		res.statusCode = 200;
		res.write(JSON.stringify(filteredCourses));
		res.end();
	});
});

const incomingNetlifyWebHook = '/api/builds/';
WebApp.connectHandlers.use(incomingNetlifyWebHook, (req, res, next) => {
	console.log(req.url);
	// deployStatus: started | succeeded | failed
	const deployStatus = req.url.split('/')[1];
	const selector = { type: 'deploy' };
	const modifier = { status: deployStatus };
	Status.upsert(selector, { $set: modifier });
	res.statusCode = 200;
	res.end();
});
